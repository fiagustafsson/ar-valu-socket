
import $ from 'jquery';
import cssFonts from './fonts.css';
import cssMain from './main.scss';
import cssDemo from './demo.scss';
import { create, update } from './main';

var $buttons, stateGroup;

function init() {
    create("viz");
    stateGroup = "tot";
    $buttons = $(".js-state-party-container button");
    $buttons.on("click", (x) => {
        let d = $(x.target);
        let p = '' + d.data("party");
        stateGroup = p;
        updateState();
    });
    
    updateState();
}

function updateState() {
    $buttons.removeClass("lit");
    let $b = $buttons.filter((x, e) => { return $(e).data("party") == stateGroup });
    $b.addClass("lit");
    let context = {
        "group": stateGroup,
        'parties': ["v", "s", "mp", "c", "l", "kd", "m", "sd"],
        'years': ["1991", "1994", "1998", "2002", "2006", "2010", "2014", "2018"]
    }
    $("#subtitle").text($b.text());
    update(context);
}


$(document).ready(function() {
    init();
})