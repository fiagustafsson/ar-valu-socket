import $ from 'jquery';
import _ from 'lodash';
import cssFonts from './fonts.css';
import cssMain from './main.scss';
import cssDemo from './demo.scss';
import { create, update } from './main';
import iconv from 'iconv-lite';
const io = require("socket.io-client");
var dataUrl = "";
var xmlUrl = "";

var socket;

var newJson;
var config;
var party = ["v","s","mp","c","l","m","kd","sd"];
var xmlData;
var $buttons, stateGroup;
var $selContainer, $selContainerSum, $selContainerParties;
var $selContainerMandate, $selContainerMandateSum, $selContainerMandateParties;
var dataLoaded = false;
var caspar = getUrlVars()["caspar"];
var color = getUrlVars()["color"];
var color2 = getUrlVars()["color2"];
var totalMandates = 0;
var oldTotalMandates = 0;
var isResult = false;
var mandatMode;
var masterID = Math.floor((Math.random() * 1000000) + 1);

function init() {
  console.log("Version 1.4");
  console.log(masterID);
  getConfig();
  setMode();
  setType();
  initSocket(config.socketUrl);
}
function setMode()
{
  var modeP = getUrlVars()["mode"];
  if (modeP == "valu" || modeP =="novus" || modeP == "resultat")
  {
    config.mode = getUrlVars()["mode"];
    mandatMode = "mandat" + config.mode;
  }
  else
  {
    mandatMode = "mandat" + config.mode;
  }
  console.log('mandatmode:' + mandatMode)
}
function onPageInit()
{
  getXML(xmlUrl, fetchData);
}

function getConfig()
{
  loadJSON('config.json', function(response) {
   // Parse JSON string into object
     config = JSON.parse(response);
  });
}

function getXML(_xmlUrl, callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open('GET', _xmlUrl, false);
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
      var xmlString = xobj.responseText;
      xmlData = new DOMParser().parseFromString(xmlString, "text/xml");
      console.log(xmlData);
      callback();
    }
    if (xobj.readyState == 4 && xobj.status == "404") {
      callback();
    }
  };
  xobj.send(null);
}

function setType() {
  switch(config.mode) {
    case "valu":
      dataUrl = config.dataUrl + "valu/valu.json";
      xmlUrl = config.dataUrl + "valu/valu.xml";
      isResult = false;
      break;
    case "novus":
      dataUrl = config.dataUrl + "novus/novus.json";
      xmlUrl = config.dataUrl + "novus/novus.xml";
      $("#logo").replaceWith("<p id='novus'>novus</p>");
      isResult = false;
      break;
    case "resultat":
      dataUrl = config.dataUrl + "resultat/data.json";
      xmlUrl = "/vds/vds.jsp?del=10000";
      $("#logo").replaceWith("<p id='novus'></p>");
      isResult = true;
      break;
    default:
      dataUrl = config.dataUrl + "valu/valu.json";
      xmlUrl = config.dataUrl + "valu/valu.xml";
      isResult = false;
      config.mode = "mandatvalu";
      break;
  }
}

function fetchData()
{
  loadJSON(dataUrl, function(response) {
    // Parse JSON string into object
    newJson = JSON.parse(response);
    setData();
  });
}

function loadJSON(url, callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open('GET', url, false);
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
      callback(xobj.responseText);
    }
  };
  xobj.send(null);
}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function xmlToJson(xml) {

  // Create the return object
  var obj = {};

  if (xml.nodeType == 1) { // element
    // do attributes
    if (xml.attributes.length > 0) {
      obj["@attributes"] = {};
      for (var j = 0; j < xml.attributes.length; j++) {
        var attribute = xml.attributes.item(j);
        obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
      }
    }
  } else if (xml.nodeType == 3) { // text
    obj = xml.nodeValue;
  }

  // do children
  if (xml.hasChildNodes()) {
    for(var i = 0; i < xml.childNodes.length; i++) {
      var item = xml.childNodes.item(i);
      var nodeName = item.nodeName;
      if (typeof(obj[nodeName]) == "undefined") {
        obj[nodeName] = xmlToJson(item);
      } else {
        if (typeof(obj[nodeName].push) == "undefined") {
          var old = obj[nodeName];
          obj[nodeName] = [];
          obj[nodeName].push(old);
        }
        obj[nodeName].push(xmlToJson(item));
      }
    }
  }
  var xmlObj = obj;
  return obj;
};

function checkIfCaspar () {
  if (caspar != "true" && caspar != "false" && caspar != "view")
  {
    caspar = "true";
  }
  if (caspar == "false") //////GLÖM EJ BYTA DENNA
  {
    var pass_entered = sessionStorage.getItem("password");
    var password="grindval";

    while (pass_entered!=password) {
    pass_entered=prompt('Lösenord krävs:','');
    }
    sessionStorage.setItem("password", pass_entered);
    document.getElementById("container").style.backgroundColor = "rgba(42,55,64,1)";
    sendMasterConnected();
    onPageInit();
  }
  else if (caspar == "true")
  {
    sendCasparConnected();
  }
  else if (caspar == "view")
  {
    document.getElementById("container").style.backgroundColor = "rgba(42,55,64,1)";
    onPageInit();
  }
  if (color == "false")
  {
    document.getElementById("container").style.backgroundColor = "rgba(0,0,0,0)";
    document.getElementById("container2").style.backgroundColor = "rgba(0,0,0,0)";
  }
  else if (color !=null || color != "false")
  {
    document.getElementById("container").style.backgroundColor = color;
    if (color2 !=null)
    {
      document.getElementById("container2").style.backgroundColor = color2;
    }
  }
}

function setData()
{

  var x = xmlData ? xmlData.getElementsByTagName("PARTI") : [];
  var i;
  var shortParty;
  var strValue;

  console.log(x);
  console.log(newJson);

  for (i = 0; i <x.length; i++) {
    shortParty = x[i].attributes.förkortning.nodeValue;
    console.log(x[i].attributes);
    if (shortParty == "V")
    {
      newJson.result.v.mandate = Number(x[i].attributes.mandat.nodeValue);
    }
    if (shortParty == "S")
    {
      newJson.result.s.mandate = Number(x[i].attributes.mandat.nodeValue);
    }
    if (shortParty == "MP")
    {
      newJson.result.mp.mandate = Number(x[i].attributes.mandat.nodeValue);
    }
    if (shortParty == "C")
    {
      newJson.result.c.mandate = Number(x[i].attributes.mandat.nodeValue);
    }
    if (shortParty == "L")
    {
      newJson.result.l.mandate = Number(x[i].attributes.mandat.nodeValue);
    }
    if (shortParty == "M")
    {
      newJson.result.m.mandate = Number(x[i].attributes.mandat.nodeValue);
    }
    if (shortParty == "L")
    {
      newJson.result.l.mandate = Number(x[i].attributes.mandat.nodeValue);
    }
    if (shortParty == "KD")
    {
      newJson.result.kd.mandate = Number(x[i].attributes.mandat.nodeValue);
    }
    if (shortParty == "SD")
    {
      newJson.result.sd.mandate = Number(x[i].attributes.mandat.nodeValue);
    }

  }
  dataIsLoaded();
}

function dataIsLoaded()
{
  /** dont work on caspar 2.07
  if (caspar == "true")
  {
    while (newJson == "") {
    }
    self.close();
  }
  **/
  create("viz", newJson);
  $("js-noclick").unbind();
  $selContainer = $("#selected-container");
  $selContainerParties = $(".selected-container-parties");
  $selContainerSum = $(".selected-container-sum");
  $selContainerMandate = $("#selected-container-mandate");
  $selContainerMandateParties = $(".selected-container-mandate-parties");
  $selContainerMandateSum = $(".selected-container-mandate-sum");
  stateGroup = [];

  $buttons = $(".js-state-party-container button");
  $buttons.on("click", (x) => {
      let d = $(x.target);
      let p = '' + d.data("party");

      if (_.includes(stateGroup, p)) {
          _.remove(stateGroup, function(x) { return x === p })

      } else {
          stateGroup.push(p);
      }
      sendToSocket();
      updateState();
  });
  updateState();
}

window.buttonOn = function buttonOn(_party)
{
  var index = stateGroup.indexOf(_party);
  if (index > -1) {
  stateGroup.splice(index, 1);
  }
  var i;
  for (i = 0; i < party.length; i++)
  {
    if (party[i] == _party)
    {
        stateGroup.push(_party);
    }
  }
  updateState();
}

window.buttonOff = function buttonOff(_party)
{
  var index = stateGroup.indexOf(_party);
  if (index > -1) {
  stateGroup.splice(index, 1);
  }
  updateState();
}


function partyPercent()
{
  var x;
  for (x = 0; x < stateGroup.length; x++)
  {
    document.getElementById(stateGroup[x] + "M").innerHTML = newJson.result[stateGroup[x]].mandate;
  }

}

function updateState() {
    $buttons.removeClass("lit");
    var $b = $buttons.filter(function() {
        return _.includes(stateGroup, $(this).data("party"));
    });
    $b.addClass("lit");
    $selContainerParties.children().remove();
    $selContainerMandateParties.children().remove();
    let sumMandates = 0;
    let sumVotes = 0;
    _.map(stateGroup, (d, ix) => {
        let $i = $('<div class="selection-item color-bg-party-' + d + '">' + d.toUpperCase() + '</div>');
        let $p = $('<div class="selection-item-text selection-item-text-space">+</div>');

        sumVotes += newJson.result[d].votes;

        sumMandates += newJson.result[d].mandate;
    });


    sumOfMandates(sumMandates);

    let context = {
        "group": stateGroup,
        'parties': ["v", "s", "mp", "c", "l", "kd", "m", "sd"],
        'years': ["1991", "1994", "1998", "2002", "2006", "2010", "2014", "2018"]
    }
    // $("#subtitle").text($b.text() + "&nbsp;");
    update(context);

    partyPercent();
}

function sumOfMandates(totalMandates) {
  var differenceMandates = Math.abs(oldTotalMandates - totalMandates);
  var test = oldTotalMandates > totalMandates ? -1 : 1;
  var time = Math.min(500 / differenceMandates, 50);
  var lastMandate;

  for(var i = 0; i <= differenceMandates; i++) {
    if (lastMandate != oldTotalMandates)
    {
      lastMandate = oldTotalMandates;
    }
    (function(i, lastMandate) {
      setTimeout(function() {
        $selContainerSum.text(lastMandate+ (i * test));
      }, time * i);
    })(i, lastMandate)
  }
  oldTotalMandates = totalMandates;
}

function countToZero()
{
  for (var y = 0; y < totalMandates; y++)
  {
    (function (y) {
    setTimeout(function () {
      if (y < (totalMandates - 1))
      {
        $selContainerSum.text((Math.floor(totalMandates - y)))
      }
      else
      {
        $selContainerSum.text("0")
      }
      }, 10*y);
    })(y);
  }
  oldTotalMandates = 0;
}

function initSocket(_url) {
  var url = _url +'?client=' + mandatMode;
  socket = io(url, {
  });

  socket.on('connect', (msg) =>
  {
    console.log("--- io connect ---");
    console.log(msg);

    checkIfCaspar();

  });

  socket.on('message', (msg) => {
    console.log(msg.event);

    if (msg.event == "newData")
    {
      var oldStateGroup = stateGroup;
      stateGroup = [];
      var x;
      for (x = 0; x < msg.data.length; x++)
      {
        stateGroup.push(msg.data[x]);
      }
      updateState();
    }
    else if(msg.event == "masterConnected")
    {
      if (caspar == "true")
      {
        location.reload(true);
      }
    }
    else if(msg.event == "casparConnected")
    {
      if (caspar == "false")
      {
        socket.send({event: 'sendNewJson', emitter: mandatMode, targets: [mandatMode] ,data:newJson});
        socket.send({event: "newData", emitter: mandatMode, targets: [mandatMode] ,data:stateGroup});
      }
    }
    else if(msg.event == "sendNewJson")
    {
      if (caspar == "true")
      {
        console.log(msg.data)
        newJson = msg.data;
        if (dataLoaded == false)
        {
          dataLoaded = true;
          dataIsLoaded();
        }
      }
    }
    else if(msg.event == "killAllMasters")
    {
      if (masterID != msg.data && caspar == "false")
      {
        caspar = "view";
        if (confirm('Ny master ansluten, du har nu fått visningsläge')) {
          var redirectUrl = window.location.href
          redirectUrl = redirectUrl.replace("caspar=false","caspar=view")
          sessionStorage.clear()
          window.location.href = redirectUrl;
        }
        else {
          alert("Du kan tyvärr inte avbryta då det finns en ny Master ansluten, du har nu fått visningsläge")
          var redirectUrl = window.location.href
          redirectUrl = redirectUrl.replace("caspar=false","caspar=view")
          sessionStorage.clear()
          window.location.href = redirectUrl;
        }
      }
    }
  });
}

function sendToSocket()
{
  if (caspar == "false")
  {
    socket.send({event: "newData", emitter: mandatMode, targets: [mandatMode] ,data:stateGroup});
  }
  console.log("TRYING TO SEND");
}

function sendMasterConnected()
{
  console.log("IM SENDING A MASTERCONNECT");
  socket.send({event: 'masterConnected', emitter: mandatMode, targets: [mandatMode] ,data:"master"});
  socket.send({event: 'killAllMasters', emitter: mandatMode, targets: [mandatMode] ,data:masterID});
}

function sendCasparConnected()
{
  console.log("IM SENDING A CASPARCONNECT");
  socket.send({event: 'casparConnected', emitter: mandatMode, targets: [mandatMode] ,data:"casparConnected"});
}


$(document).ready(function() {
    //data.result["kd"].mandate = 100;
    init();
})
