import cssMain from './main.scss';
import * as d3 from "d3";
import _ from 'lodash';

let data = require('./data.json');
const meta = require('./meta.json');

let state = [];



// set this in create
var svg, xAxis, yAxis, barContainer, innerSize, lines, markers, line, x, y;
var isCreated = false;

//settings
const margin = {
    top: 30,
    right: 30,
    bottom: 30,
    left: 40
};


function create(elementId) {

    let el = document.getElementById(elementId);
    let w = el.clientWidth;
    let h = el.clientHeight;
    innerSize = {
        width: w - margin.right - margin.left,
        height: h - margin.top - margin.bottom,
    };

    svg = d3.select(el).append("svg")
        .attr("width", w)
        .attr("height", h)
        .append("g")
        .attr("id", "svg")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    const parseDate = d3.timeParse('%Y');

    // Scales
    y = d3.scaleLinear()
        .domain([0, 100])
        .range([innerSize.height, 0]);

    x = d3.scaleTime()
        .range([0, innerSize.width]);

    const yAxis = d3.axisLeft()
        .scale(y);

    const xAxis = d3.axisBottom()
        .scale(x)
        .tickFormat((x) => {
            // media query för att ändra formatet på åren på x-axeln
            if (innerSize.width <= 400) {
                return d3.timeFormat("-%y")(x)
            } else {
                return d3.timeFormat("%Y")(x)
            }
        });

    const yAxisDOM = svg.append("g")
        .attr("class", "axis y-axis");

    const xAxisDOM = svg.append("g")
        .attr("class", "axis x-axis")
        .attr("transform", `translate(0,${innerSize.height})`);

    const yLabel = svg.append("g")
        .attr("class", "y-label")
        .append("text")
        .attr("opacity", 1)
        .attr("class", "label-text y-label-text")
        .attr("x", 18)
        .attr("y", 12)
        .attr("text-anchor", "end")
        .text("%")




    lines = svg.append("g")
        .attr("class", "lines");

    markers = svg.append("g")
        .attr("class", "markers");

    line = d3.line()
        .x(d => x(d.year))
        .y(d => y(d.value))
        .defined(d => d.value !== 0);

    data.forEach(function(d) {
        d.year = parseDate(d.year.toString());
    });


    x.domain(d3.extent(data, d => d.year));

    let uniqueYears = _.uniqBy(data.map(d => { return d.year}), (x) => { return x.getYear()});

    xAxis.tickValues(uniqueYears);

    xAxisDOM.call(xAxis);
    yAxisDOM.call(yAxis);

    isCreated = true;

}

function addLine(state) {

    let lineData = data.filter(d => d.party === state && d.value !== 0);

    lines.append("path")
        .attr("class", `party-line party-${state} party-line-${state}`)
        .datum(lineData)
        .attr("d", line)
        .attr("stroke-width", 1)
        .attr("fill", "none")
        .attr("opacity", 0)
        .transition()
        .attr("opacity", 1);

    markers
        .selectAll(`party-marker-${state}`)
        .data(lineData)
        .enter()
        .append("circle")
        .attr("class", `party-marker party-${state} party-marker-${state}`)
        .attr("cx", d => x(d.year))
        .attr("cy", d => y(d.value))
        .attr("r", 5)
        .attr("opacity", 0)
        .transition()
        .attr("opacity", 1)

}

function removeLine(state) {
    d3.selectAll(`.party-${state}`)
        .transition()
        .attr("opacity", 0)
        .remove()
}

function update(stateParties) {

    let toUpdate = _.difference(stateParties, state);
    let toRemove = _.difference(state, stateParties);
    toUpdate.forEach((x) => {addLine(x)});
    toRemove.forEach((x) => {removeLine(x)});
    state = stateParties.slice()



}


exports.create = create;
exports.update = update;