import $ from 'jquery';
import _ from 'lodash';
import cssFonts from './fonts.css';
import cssMain from './main.scss';
import cssDemo from './demo.scss';
import {renderBoard} from './templates';
import {update, create} from './main'

const rawdata = require('./data.json');

var $buttons, stateGroup;
var $selContainer, $selContainerSum, $selContainerParties;
var $selContainerMandate, $selContainerMandateSum, $selContainerMandateParties;

var $board;
var $rows; 
var data; 
var $current; 
function redraw() {
    data.rowsize = $board.width();
    $board.html(renderBoard(data));
    $(".js-clickselect").on("mousedown", e=> $current=$(e.target));
    $rows = $(".board-row");
    $rows.on("dragover",dragContainerOver );
    $rows.on("dragenter",dragContainerEnter );
    $rows.on("drop",dragContainerDrop );

    $("js-unbind").unbind();
}
function init() {
    var data2 = _.map(rawdata.result, d => _.cloneDeep(d));
    let t = 0;
    data2 = _.map(data2, d => {
      d.sz = d.mandate / 349;
      d.col = 2;
      d.top = t;
      t += d.sz;
      return d;
    });
    

    // console.log(data2);

    data = transformResults(rawdata.result);
    $board =  $("#board");
    
    redraw();
    create($(".svg-klossar").get()[0],['','Regering', 'Opposition'],data2);
    

}

function dragContainerOver(e){
    e.preventDefault();
}
function dragContainerDrop(e){
    
    let $t = $(e.target);
    if (!$t.hasClass("board-row")){
        $t = $t.parents(".board-row")
    }
    let row = $t.data("name");
    let p = $current.data("party");
    let target = _.head(_.filter(_.flatMap(data.items, "items"), d=> d.party==p));
    
    let zz = _.map(data.items,a => {
        if (a.name == row){
            a.items = _.union(a.items,[target])
        }else {
            _.remove(a.items, d=> d.party==p)
        }
        return a;
    });
    _.map(data.items, a=>
        a.total = _.reduce(_.map(a.items, d=>d.sz),(c,d) => c+d))

    redraw();

}
function dragContainerEnter(e){
    
    e.preventDefault()
}

function transformResults(obj){
    let gotMandate = _.filter(obj, d=> d.mandate > 0 );
    let notMandate = _.filter(obj, d=> d.mandate == 0 );

    let res = {
        "items":[
          {'name':"Regering",class:'row-regering', 'items': []},
          {'name':"Opposition",class:'row-opposition', 'items':[] },
          {'name':"Övriga",class:'row-ovriga', 'items':_.map(gotMandate, d=>{d.sz = Math.floor(10000*d.mandate / 349)/100;return d;})}
        ],
        'nonItemes': notMandate
    };


    return res; 
}



$(document).ready(function() {
    init();
    console.log(data);


})