import * as d3 from "d3";
import _ from 'lodash';

const data = require('./data.json');


//containers
var svg, rowContainer, legendContainer;

// axis
var xAxis, yAxis;



//state
var counterChanges = 0;

// settings
const numBars = 4;
const dur = 125; // transition duration
const xMax = 1; // values goes from 0 - 1

//      margin and paddings
var innerSize;
const xAxisBarMargin = 0;
const labelMarginRight = 0;
const margin = {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0
};

function create(elementId) {
    let el = document.getElementById(elementId);
    let w = el.clientWidth;
    let h = el.clientHeight;
    innerSize = {
        width: w - margin.right - margin.left,
        height: h - margin.top - margin.bottom,
    }

    // d3 init
    svg = d3.select(el).append("svg")
        .attr("width", innerSize.width)
        .attr("height", innerSize.height)
        .append("g")
        .attr("id", "svg")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

    rowContainer = svg.append('g').attr('class', 'rows');
    legendContainer = svg.append('g').attr('class', 'legends');

    yAxis = d3.scaleBand().rangeRound([0, innerSize.height])
        .domain(Array.from(new Array(numBars), (x, i) => i))
        .paddingInner(0.6)
        .paddingOuter(0.5);
    xAxis = d3.scaleLinear().range([0, innerSize.width - xAxisBarMargin - labelMarginRight])
        .domain([0, xMax]);

}

function update(state) {
    render(state);
    counterChanges += 1;
}


function renderLegends(fdata) {

    //only create, no updates

    var selection = legendContainer.selectAll(".legend")
        .data(fdata);
    let selectionEnter = selection
        .enter()
        .append("g")
        .attr('class', 'legend')

    selectionEnter.append("text")
        .text(function(d, i) {
            return d.year.toUpperCase()
        })
        .attr("alignment-baseline", "central")
        .attr("dominant-baseline", "central ")
        .attr("text-anchor", "right")
        .attr("y", function(d, i) {
            return yAxis(i) - yAxis.bandwidth() / 2
        }).attr("x", 0)
}

function cleanData(x) {
    x = x || 0;
    if (x == -1)
        x = 0;
    return x
}

function cleanText(x) {
    if (x > 0.10) {
        return '' + Math.round(x * 100) + ' %'
    }
    return '';
}

function render(state) {
    let fdata = data[state.group];
    renderLegends(fdata);

    let polLeft = _.map(fdata, d => {
        return {
            'text': cleanText(d.v),
            'x': 0,
            'w': cleanData(d.v),
            'classText': "pol-left-text pol-text",
            'class': "color-bg-left pol-item pol-left"
        }
    });
    let polMiddle = _.map(fdata, d => {

        return {
            'text': cleanText(d.m),
            'x': cleanData(d.v),
            'w': cleanData(d.m),
            'classText': "pol-middle-text pol-text",
            'class': "color-bg-middle  pol-item pol-middle"
        }
    });
    let polRight = _.map(fdata, d => {
        return {
            'text': cleanText(d.h),
            'x': cleanData(d.v) + cleanData(d.m),
            'w': cleanData(d.h),
            'class': "color-bg-right  pol-item pol-right",
            'classText': "pol-right-text pol-text"
        }
    });

    var rowsLeft = rowContainer.selectAll(".pol-left").data(polLeft);
    var rowsMiddle = rowContainer.selectAll(".pol-middle").data(polMiddle);
    var rowsRight = rowContainer.selectAll(".pol-right").data(polRight);

    var rowsLeftText = rowContainer.selectAll(".pol-left-text").data(polLeft);
    var rowsMiddleText = rowContainer.selectAll(".pol-middle-text").data(polMiddle);
    var rowsRightText = rowContainer.selectAll(".pol-right-text").data(polRight);


    var rows = [rowsLeft, rowsMiddle, rowsRight];
    var rowsText = [rowsLeftText, rowsMiddleText, rowsRightText];

    _.map(rows, x => {
        x.enter()
            .append("rect")
            .attr("class", d => { return d.class })
            .attr("height", yAxis.bandwidth())
            .attr("y", function(d, i) {
                return yAxis(i)
            })
            .attr("x", d => { return labelMarginRight + xAxis(d.x) })
            .attr("width", d => { return xAxis(d.w) });
    })

    _.map(rows, x => {
        x.transition()
            .duration(dur)
            .attr("x", d => { return labelMarginRight + xAxis(d.x) })
            .attr("width", d => { return xAxis(d.w) });
    })

    _.map(rowsText, x => {
        x.enter()
            .append("text")
            .attr("class", d => { return d.classText })
            .attr("height", yAxis.bandwidth())
            .attr("alignment-baseline", "central")
            .attr("dominant-baseline", "central ")
            .attr("fill-opacity", "0.8")
            .attr("fill", "#000000")
            .attr("text-anchor", "middle")
            .attr("y", function(d, i) {
                return yAxis(i) + yAxis.bandwidth() / 2
            })
            .text(d => { return d.text })
            .attr("x", d => { return labelMarginRight + xAxis(d.x) + xAxis(d.w) / 2 })
    })

    _.map(rowsText, x => {
        x.transition()
            .duration(dur)
            .attr("y", function(d, i) {
                return yAxis(i) + yAxis.bandwidth() / 2
            })
            .text(d => { return d.text })
            .attr("x", d => { return labelMarginRight + xAxis(d.x) + xAxis(d.w) / 2 })
    })

    rowContainer.selectAll(".pol-item").exit().remove();
}

exports.update = update;
exports.create = create;