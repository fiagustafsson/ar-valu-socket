import cssMain from './main.scss';
import * as d3 from "d3";

const data = require('./data.json');
const meta = require('./meta.json');

const states = meta["years"];
var stateTarget, stateCurrent = "";

// set this in create
var svg, xAxis, yAxis, barContainer, innerSize;
var isCreated = false;
var counterChanges = 0;

// settings
const numBars = 10;
const dur = 250; // transition duration
const xMax = 0.7;
const labelMarginRight = 30;
const margin = {
    top: 0,
    right: 0,
    bottom: 20,
    left: 0
};





function show(target){
    
    if (target == stateCurrent){
        return;
    }
    
    let delayEnter = dur * 2.5;
    let delayUpdate = dur * 1.5;
    let delayExit = 0;


    if (counterChanges == 0 ){
        delayEnter = 0;

    }
    render(target, delayEnter, delayUpdate, delayExit)


    counterChanges +=  1;
    stateCurrent = target;

}


function create(elementId){
    let el = document.getElementById(elementId);
    let w = el.clientWidth;
    let h = el.clientHeight;
    innerSize = {
        width: w - margin.right - margin.left,
        height: h - margin.top - margin.bottom,
    }

    // d3 init
    svg =  d3.select(el).append("svg")
        .attr("width", innerSize.width)
        .attr("height", innerSize.height)
        .append("g")
        .attr("id", "svg")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

    barContainer = svg.append('g').attr('class', 'bars');
    

    yAxis = d3.scaleBand().rangeRound([0, innerSize.height])
        .domain(Array.from(new Array(numBars), (x, i) => i))
        .paddingInner(0.3)
        .paddingOuter(0);

    xAxis = d3.scaleLinear().range([0,innerSize.width-labelMarginRight])
        .domain([0, xMax]);

    isCreated = true;
}


function render(state,delayEnter, delayUpdate, delayExit) {



    let fdata = data.filter(function(d) {
        if (d.year === state) {
            return d
        }
    });
    // make sure the data is sorted according to valuec
    fdata.sort((a, b) => b.value - a.value);
    // slice out the last n number of bars
    fdata = fdata.slice(0, numBars);



    // DATA BIND

    const bars = barContainer.selectAll(".bar")
        .data(fdata, function(d) {
            return d.question;
        })


    // UPDATE

    const update = bars.transition()
        .duration(dur)
        .delay(delayUpdate);


    update
        .select(".bar-rect")
        .attr("y", function(d, i) {
            return yAxis(i)
        })
        .attr("width", function(d) {
            return xAxis(Math.round(d.value * 100) / 100);
        });



    update
        .select(".bar-text")
        .attr("y", function(d, i) {
            return yAxis(i)
        })
        .attr("opacity", 1);

    update
        .select(".bar-num")
        .attr("y", function(d, i) {
            return yAxis(i)
        })
        .text(function(d) {
            return Math.round(d.value * 100) + "%"
        })
        .attr("dx", function(d) {
            return xAxis(Math.round(d.value * 100) / 100) + 7
        })
        .attr("opacity", 1);

    update
        .select(".bar-hover")
        .attr("y", function(d, i) {
            return yAxis(i)
        });


    // ENTER
    const enter = bars
        .enter()
        .append("g")
        .attr("class", "bar");


    enter
        .append("g")
        .attr("class", ((d, i) => "bar-rect-cont bar-rect-cont-" + i))
        .append("rect")
        .attr("class", ((d, i) => "bar-item bar-rect bar-rect-" + i))
        .attr("y", function(d, i) {
            return yAxis(i)
        })
        //.attr("height", barHeight)
        .attr("height", yAxis.bandwidth())
        .transition()
        .duration(dur)
        .delay(delayEnter)
        .attr("width", function(d) {
            return xAxis(Math.round(d.value * 100) / 100);
        });



    enter
        .select(".bar-rect-cont")
        .append("text")
        .attr("class", ((d, i) => "bar-item bar-text bar-text-" + i))
        .attr("y", function(d, i) {
            return yAxis(i)
        })
        //                    .attr("dy", (barHeight/2 + barNumOffset))
        .attr("dy", function() {
            // sätter relativa positionen för texten utifrån bar-storleken och font-storleken
            const el = document.getElementsByClassName("bar-text")[0];
            const style = window.getComputedStyle(el).getPropertyValue('font-size');
            const fontSize = parseFloat(style);
            //console.log(y.bandwidth()/2, fontSize)
            return (yAxis.bandwidth() / 2) + (fontSize / 3)
        })
        //                    .style("font-size", fontSize)
        .attr("opacity", 0)
        .text(function(d) {
            return d.question;
        })
        .transition()
        .duration(dur)
        .delay(delayEnter)
        .attr("opacity", 1)
        .attr("dx", 6);

    enter
        .select(".bar-rect-cont")
        .append("text")
        .attr("class", ((d, i) => "bar-item bar-num bar-num-" + i))
        .attr("y", function(d, i) {
            return yAxis(i)
        })
        //                    .attr("dy", (barHeight/2 + barNumOffset))
        .attr("dy", function() {
            // sätter relativa positionen för texten utifrån bar-storleken och font-storleken
            const el = document.getElementsByClassName("bar-text")[0];
            const style = window.getComputedStyle(el).getPropertyValue('font-size');
            const fontSize = parseFloat(style);
            //console.log(y.bandwidth()/2, fontSize)
            return (yAxis.bandwidth() / 2) + (fontSize / 3)
        })
        .attr("opacity", 0)
        .text(function(d) {
            return Math.round(d.value * 100) + "%"
        })
        .transition()
        .duration(dur)
        .delay(delayEnter)
        .attr("opacity", 1)
        .attr("dx", function(d) {
            return xAxis(Math.round(d.value * 100) / 100) + 7
        });


    enter
        .append("rect")
        .attr("class", "bar-hover")
        .attr("opacity", 0)
        .attr('fill', 'lightgrey')
        .attr("y", function(d, i) {
            return yAxis(i);
        })
        .attr("transform", "translate(0,-" + (yAxis.bandwidth() + yAxis.step() * yAxis.paddingInner()) / 2 + ")")
        .attr("width", innerSize.width)
        .attr("height", yAxis.bandwidth() + yAxis.step() * yAxis.paddingInner())

    // EXIT

    const exit = bars.exit()
        .transition()
        .duration(dur)
        .delay(delayExit);

    exit
        .select(".bar-rect")
        .attr("width", 0);


    exit
        .select(".bar-text")
        .attr("opacity", 0);

    exit
        .select(".bar-num")
        .attr("dx", 0)
        .attr("opacity", 0);

    exit
        .remove();
}




export default () => {
        return {
            "states": states,
            "render": render
        }
};
exports.states = states;
exports.show = show;
exports.create = create;
