import io
import mimetypes
import os

from minio import Minio
from minio.error import ResponseError


class Miniotarus(object):
    def __init__(self, config):
        self.mc = Minio(
            config.get("endpoint"),
            access_key=config.get("access_key"),
            secret_key=config.get("secret_key"),
            secure=True,
        )

    def buckets(self):
        return [x.name for x in self.mc.list_buckets()]

    def walk(self, bucket, prefix="",recursive=True):
        for z in self.mc.list_objects_v2(bucket, prefix, recursive=recursive):
            yield z.__dict__

    def fput(self, bucket, key, fp, content_type="application/octet-stream"):
        self.mc.fput_object(bucket, key, fp, content_type)

    def putdir(self, bucket, prefix, localdir):
        zz = len(localdir)
        for root, _, files in os.walk(localdir):
            for f in files:
                fp = os.path.join(root, f)
                key = os.path.join(prefix, fp[zz + 1:])
                mimetype = mimetypes.guess_type(fp)[0]
                self.fput(bucket, key, fp, mimetype)
