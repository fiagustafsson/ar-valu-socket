import cssMain from './main.scss';
import * as d3 from "d3";
import _ from 'lodash';

const data = require('./data.json');

var stateTarget, stateCurrent = "";

// set this in create
var svg, xAxis, yAxis, barContainer, innerSize, barContainer, partyBoxContainer;
var isCreated = false;
var counterChanges = 0;
var inUpdate = false;

const xAxisBarMargin = 50;
// settings
const numBars = 10;
const dur = 125; // transition duration
const xMax = 0.7;
const labelMarginRight = 30;
const margin = {
    top: 0,
    right: 0,
    bottom: 20,
    left: 0
};





function update(state) {
    if (inUpdate) {
        return
    }
    inUpdate = true;

    let delayEnter = dur * 2.5;
    let delayUpdate = dur * 1.5;
    let delayExit = 0;


    if (counterChanges == 0) {
        delayEnter = 0;

    }
    render(state, delayEnter, delayUpdate, delayExit)
    counterChanges += 1;
    stateCurrent = state;
    inUpdate = false;
}



function create(elementId) {
    let el = document.getElementById(elementId);
    let w = el.clientWidth;
    let h = el.clientHeight;
    innerSize = {
        width: w - margin.right - margin.left,
        height: h - margin.top - margin.bottom,
    }

    // d3 init
    svg = d3.select(el).append("svg")
        .attr("width", innerSize.width)
        .attr("height", innerSize.height)
        .append("g")
        .attr("id", "svg")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")")

    barContainer = svg.append('g').attr('class', 'bars');
    partyBoxContainer = svg.append('g').attr('class', 'partybox');


    yAxis = d3.scaleBand().rangeRound([0, innerSize.height])
        .domain(Array.from(new Array(numBars), (x, i) => i))
        .paddingInner(0.3)
        .paddingOuter(0);
    xAxis = d3.scaleLinear().range([0, innerSize.width])
        .domain([0, xMax]);

    isCreated = true;
}


function render(state, delayEnter, delayUpdate, delayExit) {


    let fdata = _.map(state.parties, (x) => {
        return _.head(_.filter(data, (d) => {
            let q1 = (d.party === x);
            let q2 = (d.issue === state.group);
            return q1 && q2;
        }));
    });

    fdata = _.map(fdata, (x) => {
        let r = {
            value: x.value,
            id: x.party
        }
        if (r.value == -1) {
            r.text = "Uppgift saknas",
                r.value = 0
        } else {
            r.text = '' + Math.round(r.value * 100) + "%";
        }
        return r;

    })

    // DATA BIND
    const partyBoxes = partyBoxContainer.selectAll(".partybox")
        .data(state.parties, function(d) {
            return d;
        })

    const bars = barContainer.selectAll(".bar")
        .data(fdata, function(d) {
            return d.id;
        })


    // UPDATE

    const update = bars.transition()
        .duration(dur);
    // .delay(delayUpdate);


    update
        .select(".bar-rect")
        .attr("y", function(d, i) {
            return yAxis(i)
        })

        .attr("width", function(d) {
            return xAxis(Math.round(d.value * 100) / 100);
        });

    update
        .select(".text-value")
        .attr("x", function(d) {
            if (d.value > 0) {

                return xAxis(Math.round(d.value * 100) / 100) + xAxisBarMargin + 10;
            }
            return xAxis(0) + xAxisBarMargin;
        })
        .text(function(d) {
            return d.text;
        })




    // ENTER
    const enterPartybox = partyBoxes
        .enter()
        .append("g")
        .attr("class", "partybox");

    enterPartybox.append("rect")
        .attr("class", ((d, i) => "color-bg-party-" + d))
        .attr("y", function(d, i) {
            return yAxis(i)
        })
        .attr("height", yAxis.bandwidth())
        .attr("width", yAxis.bandwidth());

    enterPartybox.append("text")

        .attr("y", function(d, i) {
            return yAxis(i)
        })
        .text(function(d, i) {
            return d.toUpperCase()
        })
        .attr("alignment-baseline", "central")
        .attr("dominant-baseline", "central")
        .attr("text-anchor", "middle")
        .attr("y", function(d, i) {
            return yAxis(i) + yAxis.bandwidth() / 2
        })
        .attr("x", yAxis.bandwidth() / 2)

    const enter = bars
        .enter()
        .append("g")
        .attr("class", "bar");


    let g1 = enter.append("g");

    g1.attr("class", ((d, i) => "bar-rect-cont bar-rect-cont-" + i + " color-bg-party-" + d.id))
        .append("rect")
        .attr("class", ((d, i) => "bar-item bar-rect bar-rect-" + i))
        .attr("y", function(d, i) {
            return yAxis(i)
        })
        .attr("x", xAxisBarMargin)
        //.attr("height", barHeight)
        .attr("height", yAxis.bandwidth())
        .transition()
        .duration(dur)
        .delay(delayEnter)
        .attr("width", function(d) {
            return xAxis(Math.round(d.value * 100) / 100);
        });

    g1.append("text")

        .attr("class", "text-value")
        .attr("y", function(d, i) {
            return yAxis(i)
        })
        // .attr("x", function(d) {
        // return xAxis(Math.round(d.value * 100) / 100) + xAxisBarMargin + 10;
        // })
        .attr("x", function(d) {
            if (d.value > 0) {

                return xAxis(Math.round(d.value * 100) / 100) + xAxisBarMargin + 10;
            }
            return xAxis(0) + xAxisBarMargin;
        })
        .attr("height", yAxis.bandwidth())
        .attr("alignment-baseline", "central")
        .attr("dominant-baseline", "central")
        .attr("text-anchor", "right")
        .attr("y", function(d, i) {
            return yAxis(i) + yAxis.bandwidth() / 2
        })
        .text(x => { return x.text });
    // EXIT

    const exit = bars.exit()
        .transition()
        .duration(dur)
        .delay(delayExit);

    exit
        .select(".bar-rect")
        .attr("width", 0);
    exit
        .remove();
}




exports.update = update;
exports.create = create;