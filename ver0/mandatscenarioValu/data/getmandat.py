import subprocess
from bs4 import BeautifulSoup
import pandas as pd
import json
import datetime


def get_url(m, c, l, kd, s, v, mp, sd, fi=0):
    s = f'http://data.val.se/valsimulator/?t%C3%A4=349&envk=1&v.0.r.1={m}&v.0.r.2={c}&v.0.r.3={l}&v.0.r.4={kd}&v.0.r.5={s}&v.0.r.6={v}&v.0.r.7={mp}&v.0.r.8={sd}&v.0.r.9995=0&v.0.r.9={fi}&y=2700'
    return s


def get_mandat(scenario, name=""):
    url = get_url(**scenario)
    blob = get_page(url)
    soup = BeautifulSoup(blob, "html5lib")
    df = pd.read_html(str(soup.find("section", {
        "id": "summa_mandat"
    }).table))[0]

    df.columns = [x.lower() for x in df.columns]
    df = df.rename(columns={'fp': 'l'})
    del df["valkrets"]
    del df["total"]
    obj = df.to_dict("records")[0]
    res = {}
    for k, v in scenario.items():
        res[k] = {'party': k, 'votes': v / 10, 'mandate': obj.get(k, 0)}

    result = {
        'name': name,
        'url': url,
        'result': res,
        'dt': datetime.datetime.now().isoformat(),
        'total_mandate': sum([x["mandate"] for x in res.values()]),
        'total_votes': sum([x["votes"] for x in res.values()])
    }
    return result


def get_page(url):
    z = subprocess.check_output(
        ["google-chrome", "--headless", "--dump-dom", url])
    # print(z)
    return z


def main():
    scen = {
        'm': 237,
        'c': 88,
        'l': 46,
        'kd': 27,
        's': 284,
        'v': 81,
        'mp': 43,
        'sd': 171,
        'fi': 0,
    }
    d = get_mandat(scen)
    d = eval(str(d))
    print(json.dumps(d, indent=1, ensure_ascii=False, sort_keys=True))


if __name__ == '__main__':
    main()
