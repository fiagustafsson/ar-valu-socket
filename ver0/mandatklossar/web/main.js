import * as d3 from "d3";
import _ from 'lodash';
import { colors } from '../../../styles/basevalu';

//containers
var svg;

//state
var counterChanges = 0;
var data;
var innerSize;

const layout = {
    viewboxX: 640,
    ratio: 1/1.618
}
var hScale,xScale;

var clickIndex = 0;
var dx0, dy0;

function create(el, scenario, indata) {
    data = _.sortBy(_.map(indata, d => {
        d.ix = clickIndex;
        clickIndex += 1;
        return d;
    }), d=> d.ix);
    updateSort();
    

    // data = dd;
    // console.log(data);
    let w = el.clientWidth;
    let h = el.clientHeight;
    innerSize = {
        width: w ,
        height: h,
    }

    // d3 init
    svg = d3.select(el).append("svg")
        .attr("viewBox", "0 0 " + layout.viewboxX + " " + layout.viewboxX * layout.ratio)
        .attr("width", w)
        .attr("height", w * layout.ratio)
        // .attr("transform", "rotate(90)")
        .append("g")

    hScale = d3.scaleLinear()
        .range([0, layout.viewboxX * layout.ratio])
        .domain([0, 1]);    
    xScale = d3.scaleLinear()
        .range([0, layout.viewboxX])
        .domain([0, 3]);


    var sitems = svg.selectAll(".box").data(data);
    sitems.enter()
        .append("rect")
        .attr("width", function(d, i) {
            return xScale(0.8);
        }) 
        .attr("height", function(d, i) {
            return hScale(d.sz);
        })
        .attr("x", function(d, i) {
            return xScale(d.col);
        })        
        .attr("y", function(d, i) {
            return hScale(d.top);
        })
        .attr("class", d => { return "box" })
        .style("fill", d => { return colors.party[d.party] });

         svg.selectAll(".box").call(d3.drag()
        .on("start", dragstarted)
        .on("drag", dragdrag)
        .on("end", dragend));

        function dragdrag() {
        var d = d3.event.subject,
              x = d3.event.x,
              y = d3.event.y;
        let dx = x-dx0;
        let dy = y-dy0;
         d3.select(this).attr("transform",  "translate(" + dx + "," + dy + ")")
        } 
        function dragstarted() {
            dx0 = d3.event.x;
            dy0 = d3.event.y;
        }
        function dragend(){
            var d = d3.event.subject,
            x = d3.event.x,
            y = d3.event.y;
            d.ix = clickIndex;
            clickIndex += 1;
            d.col = Math.floor(xScale.invert(x));
            updateSort();
            redraw();
        }
}

function updateSort(){
    
    let ii = _.sortBy(data,b => b.ix);
    let t0 = [0,0,0];
    _.map(ii, d=> {
        d.top =  t0[d.col];
        t0[d.col] += d.sz;
    })
}

function redraw(){
    
    svg.selectAll(".box")
    .data(data)
    .attr("transform", "")
    .attr("x", function(d, i) {
        return xScale(d.col);
    })        
    .attr("y", function(d, i) {
        return hScale(1-d.top)-hScale(d.sz);
    })

}
function update(state) {
    render(state);
    counterChanges += 1;
}


function render(state) {
    let ar = data["result"][state.group];
    let z = [];
    let xx = _.map(state.group, d => {
        let p = data["result"][d];
        p["n"] = 0;
        return p
    });
    xx = _.filter(xx, d => d.mandate > 0);
    let ix = 0;
    let fdata = _.map(mandates, (d, i) => {
        d.party = "-";
        d.class = ""
        if (xx.length > ix) {
            let z0 = xx[ix];
            if (z0.n < z0.mandate) {
                z0.n += 1;
                d.party = z0.party;
            }
            if (z0.n == z0.mandate) {
                ix += 1;
            }

        }
        return d;
    })
    var sMandate = mContainer.selectAll(".mandate").data(fdata);
    sMandate.enter()
        .append("circle")
        .attr("r", radius)
        // .attr("stroke", "#ffffff10")
        // .attr("stroke-width", "1")
        .attr("cy", function(d, i) {
            return gridScale(d.row);
        })
        .attr("cx", function(d, i) {

            return gridScale(d.col);
        })
        .attr("class", d => { return "mandate" })
        .style("fill", d => { return colors.party[d.party] });
    sMandate.transition().style("fill", d => { return colors.party[d.party] });
    sMandate.selectAll(".mandate").exit().remove();
}

exports.update = update;
exports.create = create;