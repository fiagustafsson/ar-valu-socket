import _ from 'lodash';
import $ from 'jquery';
import cssFonts from './fonts.css';
import * as d3 from "d3";
import {create, initValjarstrom, update} from './main';

var io = require("socket.io-client");
var socket;
var client;

var state = {position: "", party: "", id: ""};
export var isCaspar = false;
export var isViewMode = false;
var data;
var config;
var color = "rgba(45,55,64,0.75)";

//CONTROL FROM CASPAR
window.buttonOn = function buttonOn(_party, _position) {
  state = {party: _party, position:_position};
  update(_party, _position);
}

//SOCKET
function initSocket(socketUrl) {
  var _url = socketUrl + '?client=valjarstrommar';
  socket = io(_url, {
  });

  socket.on('connect', (msg) => {
    console.log("--- io connect ---");
    if(!isCaspar && !isViewMode) {
      state.id += +new Date();
      socket.send({event: 'clear', emitter: "valjarstrommar", targets: ['*'],data:state});
      socket.send({event: 'killAllMasters', emitter: "valjarstrommar", targets: ['*'] ,data:state});
    }
    if(isCaspar)
      socket.send({event: 'getState', emitter: "valjarstrommar", targets: ['*']});
    console.log(msg);
  });

  socket.on('message', (msg) => {
    console.log("--- io message ---");

    if (msg.event == "valjarstrom")
    {
      var _state = {};
      if(msg.data && state.id != msg.data.id && !isViewMode) {
        _state = {party: msg.data.party, position: msg.data.position, id: msg.data.id};
        state = _state;
        update(_state.party, _state.position, false);
      }
    } else if(msg.event == "clear" && isCaspar) {
      console.log(msg);
      location.reload(true);
    } else if(msg.event == "getState" && !isCaspar) {
      console.log("send state");
      if(state.id != "")
        sendToSocket(state);
    } else if(msg.event == "killAllMasters") {
      if (state.id != msg.data.id && !isCaspar)
      {
        isViewMode = true;
        if (confirm('Ny master ansluten, du har nu fått visningsläge')) {
          var redirectUrl = window.location.href;
          redirectUrl = redirectUrl.replace("caspar=false","view=true");
          sessionStorage.clear();
          window.location.href = redirectUrl;
        }
        else {
          var redirectUrl = window.location.href;
          redirectUrl = redirectUrl.replace("caspar=false","view=true");
          sessionStorage.clear();
          window.location.href = redirectUrl;
        }
      }
    }
  });
}

function sendToSocket(_state) {
  if(!isViewMode && !isCaspar) {
    state = _state;
    if(state.id != "")
      state.id = +new Date();
    else
      state.id += +new Date();
    console.log("sent", state);
    console.log("---TRYING TO SEND---");
    socket.send({event: 'valjarstrom', emitter: "valjarstrommar", targets: ['*'],data:state});
  }
}


//ANNAT
function getUrlVar(params, paramString) {
  if(params.indexOf(paramString) > -1) {
    return params.split(paramString).pop().split("&").shift();
  }
}

function getConfig()
{
  loadJSON('config.json', function(response) {
    // Parse JSON string into object
    config = JSON.parse(response);

    loadJSON(config.dataUrl + 'valjarstrommar.json', function(_data) {
      data = JSON.parse(_data);
    });
  });
}

function init() {
  console.log("Version 1.5");
  getConfig();

  var _url = window.location.href;
  var params = _url.split("index.html?").pop();
  isCaspar = getUrlVar(params,"caspar=") !== "false";
  isViewMode = getUrlVar(params, "view=") === "true";
  color = getUrlVar(params, "color=") ? getUrlVar(params, "color=") : !isCaspar ? "rgba(45,55,64,1)" : color;

  $("#content").css("background-color", color);

  if(!isCaspar && !isViewMode) {
    var pass_entered = sessionStorage.getItem("password");
    var password = "grindval";

    while(pass_entered != password) {
      pass_entered = prompt("Lösenord krävs:", "");
    }
    sessionStorage.setItem("password", pass_entered);
  }

  initSocket(config.socketUrl);
  initValjarstrom(config.partiesToInclude, config.nodesNameFrom, config.nodesNameTo);
  create("viz", data);
}

function loadJSON(_url, callback) {
  var xobj = new XMLHttpRequest();
  xobj.overrideMimeType("application/json");
  xobj.open('GET', _url, false);
  xobj.onreadystatechange = function () {
    if (xobj.readyState == 4 && xobj.status == "200") {
      callback(xobj.responseText);
    }
  };
  xobj.send();
}

window.onload = init;
window.onresize = function() {create("viz", data)};

exports.sendToSocket = sendToSocket;
