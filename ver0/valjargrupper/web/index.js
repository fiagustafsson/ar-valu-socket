import _ from 'lodash';
import $ from 'jquery';
import cssFonts from './fonts.css';
import * as d3 from "d3";
import { create, update } from './main';
create("viz");

var stateYear = "2018";
var stateGroup = "women";

var $buttonsYear, $selectGroup;

function init() {
    $buttonsYear = $(".btn-group-year button");
    $selectGroup = $(".js-select-group");

    $buttonsYear.on("click", (x) => {
        let d = $(x.target);
        stateYear = '' + d.data("year");
        updateState();
    });
    $selectGroup.on("change", (x) => {
        let $d = $(x.target);
        stateGroup = '' + $d.val();
        updateState();
    });
    $selectGroup.val(stateGroup);
    updateState();

}

function updateState() {
    $buttonsYear.removeClass("lit");

    var $activeButtonYear = $buttonsYear.filter(function() {
        return $(this).data("year") == stateYear;
    });
    $activeButtonYear.addClass("lit");

    let context = {
        "year": stateYear,
        "group": stateGroup,
        'parties': ["v", "s", "mp", "c", "l", "kd", "m", "sd"],
        'years': [1991, 1994, 1998, 2002, 2006, 2010, 2014, 2018]
    }
    $("#subtitle").text($selectGroup.find(":selected").text() + ", " + $activeButtonYear.text());
    update(context);
}


$(document).ready(function() {
    init();
})