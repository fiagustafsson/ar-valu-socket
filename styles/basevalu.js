var colors = {
    party: {
        "-": "#d2dcdc",
        "v": "#911414",
        "s": "#e03141",
        "mp": "#82c782",
        "c": "#31a431",
        "l": "#1e69aa",
        "m": "#7cbde0",
        "kd": "#00006d",
        "sd": "#ffc346",
        "fi": "#b2367e",
        "ovr": "#8c8c8c",
    }
}


exports.colors = colors;