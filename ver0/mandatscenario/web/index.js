import $ from 'jquery';
import _ from 'lodash';
import cssFonts from './fonts.css';
import cssMain from './main.scss';
import cssDemo from './demo.scss';
import { create, update } from './main';
import iconv from 'iconv-lite';
//import http	= require( 'http' );
const data = require('./data.json');
//Object.defineProperty(exports, "__esModule", { value: true });
//const http = require("http");
const io = require("socket.io-client");
//var url = 'http://10.21.81.159:8080?client=template';
var url = 'http://' + window.location.hostname +':8080?client=mandat';
//const vdsurl = 'https://randomuser.me/api/?results=10'; FUNKAR ATT fetchData
const vdsurl = '/vds/vds.jsp?del=10000';
const socket = io(url, {
});

var newJson;
var party = ["v","s","mp","c","l","m","kd","sd"];
var srcXml;
var $buttons, stateGroup;
var $selContainer, $selContainerSum, $selContainerParties;
var $selContainerMandate, $selContainerMandateSum, $selContainerMandateParties;
var dataLoaded = false;

var caspar = getUrlVars()["caspar"];
var test = "";
function init() {
  getUrlVars();
  checkIfCaspar();
}

function onPageInit()
{
  getXML(vdsurl).then(function(obj){
    fetchData();
  })
}



function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

function checkIfCaspar () {
  if (caspar != "true" && caspar != "false")
  {
    caspar = "true";
  }
  var browser = String(navigator.userAgent);
  if (caspar == "false") //////GLÖM EJ BYTA DENNA
  {
    var pass_entered;
    var password="grindval";

    while (pass_entered!=password) {
    pass_entered=prompt('Lösenord krävs:','');
    }
    self.close();
    document.getElementById("container").style.backgroundColor = "#2a3740";
    document.body.style.background = '#2a3740';
    sendMasterConnected();
    onPageInit();
  }
  else if (caspar == "true")
  {
    sendCasparConnected();
  }
  /**
  if (browser.search("Chrome/33.0.1750.170") > -1) //////GLÖM EJ BYTA DENNA
  {
    caspar = true;
  }
  else
  {
    caspar = false;
    document.getElementById("container").style.backgroundColor = "#2a3740";
  }
  **/
  /**
  if(window.self !== window.top) { // we are in the iframe
    console.log("IM IN AN IFRAME")
  } else { // not an iframe
    document.getElementById("container").style.backgroundColor = "lightblue";
    console.log("NOT IN IFRAME");
  }
  **/
}

function dataIsLoaded()
{
  /**
  if (caspar == "true")
  {
    while (newJson == "") {
    }
    self.close();
  }
  **/
  create("viz", newJson);
  $("js-noclick").unbind();
  $selContainer = $("#selected-container");
  $selContainerParties = $(".selected-container-parties");
  $selContainerSum = $(".selected-container-sum");
  $selContainerMandate = $("#selected-container-mandate");
  $selContainerMandateParties = $(".selected-container-mandate-parties");
  $selContainerMandateSum = $(".selected-container-mandate-sum");
  stateGroup = [];

  $buttons = $(".js-state-party-container button");
  $buttons.on("click", (x) => {
      let d = $(x.target);
      let p = '' + d.data("party");

      if (_.includes(stateGroup, p)) {
          _.remove(stateGroup, function(x) { return x === p })

      } else {
          stateGroup.push(p);
      }
      sendToSocket();
      updateState();
  });
  updateState();
}

window.buttonOn = function buttonOn(_party)
{
  var index = stateGroup.indexOf(_party);
  if (index > -1) {
  stateGroup.splice(index, 1);
  }
  var i;
  for (i = 0; i < party.length; i++)
  {
    if (party[i] == _party)
    {
        stateGroup.push(_party);
    }
  }
  //console.log(stateGroup)
  updateState();
}

window.buttonOff = function buttonOff(_party)
{
  var index = stateGroup.indexOf(_party);
  if (index > -1) {
  stateGroup.splice(index, 1);
  }
  updateState();
}

function fetchData()
{
  loadJSON(function(response) {
   // Parse JSON string into object
     newJson = JSON.parse(response);
      setData();

  });
}

function setData()
{

  var x = srcXml.getElementsByTagName("PARTI");
  var i;
  var shortParty;
  var strValue;
  for (i = 0; i <x.length; i++) {
    shortParty = x[i].attributes.förkortning.nodeValue;
    if (shortParty == "V")
    {
      newJson.result.v.mandate = Number(x[i].attributes.mandat.nodeValue);
    }
    if (shortParty == "S")
    {
      newJson.result.s.mandate = Number(x[i].attributes.mandat.nodeValue);
    }
    if (shortParty == "MP")
    {
      newJson.result.mp.mandate = Number(x[i].attributes.mandat.nodeValue);
    }
    if (shortParty == "C")
    {
      newJson.result.c.mandate = Number(x[i].attributes.mandat.nodeValue);
    }
    if (shortParty == "L")
    {
      newJson.result.l.mandate = Number(x[i].attributes.mandat.nodeValue);
    }
    if (shortParty == "M")
    {
      newJson.result.m.mandate = Number(x[i].attributes.mandat.nodeValue);
    }
    if (shortParty == "L")
    {
      newJson.result.l.mandate = Number(x[i].attributes.mandat.nodeValue);
    }
    if (shortParty == "KD")
    {
      newJson.result.kd.mandate = Number(x[i].attributes.mandat.nodeValue);
    }
    if (shortParty == "SD")
    {
      newJson.result.sd.mandate = Number(x[i].attributes.mandat.nodeValue);
    }

  }
  dataIsLoaded();
  //console.log(xmlObj.data[0])
}

function loadJSON(callback) {

   var xobj = new XMLHttpRequest();
       xobj.overrideMimeType("application/json");
   xobj.open('GET', '/valdata/mandatscenario/newData.json', false);
   xobj.onreadystatechange = function () {
         if (xobj.readyState == 4 && xobj.status == "200") {
           // Required use of an anonymous callback as .open will NOT return a value but simply returns undefined in asynchronous mode
           callback(xobj.responseText);
         }
   };
   xobj.send(null);
}


function partyPercent()
{
  var x;
  for (x = 0; x < party.length; x++)
  {
    document.getElementById(party[x]).innerHTML ="";
    document.getElementById(party[x] + "M").innerHTML ="";
  }

  for (x = 0; x < stateGroup.length; x++)
  {
    document.getElementById(stateGroup[x]).innerHTML = percentCalc(stateGroup[x]);
    document.getElementById(stateGroup[x] + "M").innerHTML = newJson.result[stateGroup[x]].mandate + " mandat";
  }

}

function percentCalc(_party)
{
  return (Math.round(1000 * (newJson.result[_party].mandate) / 349) / 10 + "%");
}

function updateState() {
    $buttons.removeClass("lit");
    var $b = $buttons.filter(function() {
        return _.includes(stateGroup, $(this).data("party"));
    });
    $b.addClass("lit");
    $selContainerParties.children().remove();
    $selContainerMandateParties.children().remove();
    let sumMandates = 0;
    _.map(stateGroup, (d, ix) => {
        let $i = $('<div class="selection-item color-bg-party-' + d + '">' + d.toUpperCase() + '</div>');
        let $p = $('<div class="selection-item-text selection-item-text-space">+</div>');
        // $selContainerParties.append($i);
        // if (ix != stateGroup.length - 1) {
        //     $selContainerParties.append($p);
        // }

        sumMandates += newJson.result[d].mandate;
    });

    $selContainerSum.text("0%");
    $selContainerMandateSum.text("");
    if (stateGroup.length > 0) {
        $selContainerSum.text(Math.round(1000 * sumMandates / 349) / 10 + "%");
    }


    if (stateGroup.length > 0) {
        $selContainerMandateSum.text('= ' + sumMandates + "/ 349");
    }
    let context = {
        "group": stateGroup,
        'parties': ["v", "s", "mp", "c", "l", "kd", "m", "sd"],
        'years': ["1991", "1994", "1998", "2002", "2006", "2010", "2014", "2018"]
    }
    // $("#subtitle").text($b.text() + "&nbsp;");
    update(context);

    partyPercent();
}

socket.on('connect', (msg) =>
   {
     console.log("--- io connect ---");
     console.log(msg);

     // console.log("--- socket ---" + this.socket);
     // this.socket.send(new DataEvent("testevent", ["*"], { type: ApplicationViewModel.TYPE_4OF, data: { image: "", options: [ { option: "opt1", selected:false, correct:true}, { option: "opt2", selected:false, correct:true} ] } } ));
   });

socket.on('message', (msg) => {
         console.log(msg.event);
         console.log("AM I A CASPAR?")
         console.log(caspar);
          if (msg.event == "mandat")
          {
          stateGroup = [];
           var x;
           for (x = 0; x < msg.data.length; x++)
           {
             stateGroup.push(msg.data[x]);
           }
           updateState();
         }
         else if(msg.event == "masterConnected")
         {
           if (caspar == "true")
           {
             location.reload();
           }
         }
         else if(msg.event == "casparConnected")
         {
           if (caspar == "false")
           {
             console.log("IM SETTING NEW DATA?")
             console.log(caspar);
             socket.send({event: 'sendNewJson', emitter: "mandat", targets: ['*'] ,data:newJson});
           }
         }
         else if(msg.event == "sendNewJson")
         {
           if (caspar == "true")
           {
             console.log("I GOT DATA?");
             newJson = msg.data;
             if (dataLoaded == false)
             {
               dataLoaded = true;
               dataIsLoaded();
             }
           }
         }
         });



function sendToSocket()
{
  if (caspar == "false")
  {
    socket.send({event: 'mandat', emitter: "mandat", targets: ['*'] ,data:stateGroup});
  }
  console.log("TRYING TO SEND");
}

function sendMasterConnected()
{
  console.log("IM SENDING A MASTERCONNECT");
  socket.send({event: 'masterConnected', emitter: "mandat", targets: ['*'] ,data:"master"});
}

function sendCasparConnected()
{
  console.log("IM SENDING A CASPARCONNECT");
  socket.send({event: 'casparConnected', emitter: "mandat", targets: ['*'] ,data:"casparConnected"});
}
////////////////////////////////////////////// FETcH DATA VDS /////////////////////////////////////////////////////
/**
function getXML(vdsurl) {
  return fetch(vdsurl).then((response) => response.text())
  .then(function(response) {
    var src = iconv.decode(new Buffer(response), 'iso-8859-1').toString();
    console.log(src)
    xmlConvert(src) /////////////////FÖRSÖKER PARSA OM TILL XML JUST NU BLIR OBJ TOM
    })
}
**/
function getXML(vdsurl) {
  return fetch(vdsurl).then(function(response) {
    return response.arrayBuffer();
  })
  .then(function(arrayBuffer) {
    var src = iconv.decode(new Buffer(arrayBuffer), 'iso-8859-1').toString();
    srcXml = (new window.DOMParser()).parseFromString(src, 'text/xml');
    xmlToJson(srcXml) /////////////////FÖRSÖKER PARSA OM TILL XML JUST NU BLIR OBJ TOM
    })
}
/////var tempBuffer = new Buffer(response, 'iso-8859-1');
////var iconv = new Iconv('ISO-8859-1', 'utf8');
//////var tempBuffer = iconv.convert(tempBuffer);
//console.log(iconv);
/**
function getXML(vdsurl) {
  return fetch(vdsurl).then(function(response) {
    // Read buffer
    return response.arrayBuffer();
  }).then(function(arrayBuffer) {
    // Encoding to UTF-8
    return iconv.decode(new Buffer(arrayBuffer), 'iso-8859-1').toString();
  }).then(function(xmlStr) {
    // Parsing to HTML Document
    return (new window.DOMParser()).parseFromString(xmlStr, 'text/xml');
  }).catch(function(error) {
    console.error(error);
  });
};

**/
function xmlConvert(_data)
{
  var xml = $.parseXML(_data),
  $xml = $( xml )
  xmlToJson($xml)
}


function xmlToJson(xml) {

	// Create the return object
  var obj = {};

	if (xml.nodeType == 1) { // element
		// do attributes
		if (xml.attributes.length > 0) {
		obj["@attributes"] = {};
			for (var j = 0; j < xml.attributes.length; j++) {
				var attribute = xml.attributes.item(j);
				obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
			}
		}
	} else if (xml.nodeType == 3) { // text
		obj = xml.nodeValue;
	}

	// do children
	if (xml.hasChildNodes()) {
		for(var i = 0; i < xml.childNodes.length; i++) {
			var item = xml.childNodes.item(i);
			var nodeName = item.nodeName;
			if (typeof(obj[nodeName]) == "undefined") {
				obj[nodeName] = xmlToJson(item);
			} else {
				if (typeof(obj[nodeName].push) == "undefined") {
					var old = obj[nodeName];
					obj[nodeName] = [];
					obj[nodeName].push(old);
				}
				obj[nodeName].push(xmlToJson(item));
			}
		}
	}
  xmlObj = obj;
	return obj;
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$(document).ready(function() {
    //data.result["kd"].mandate = 100;
    init();
})
