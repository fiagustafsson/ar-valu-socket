import asyncio
import sys
import time

import argument
from pyppeteer import launch


async def screenshot(url, dst, width, height, scalefactor, is_mobile, fullpage,
                     delay):
    browser = await launch({"headless": True})
    page = await browser.newPage()
    await page.setViewport({
        'width': width,
        "height": height,
        "deviceScaleFactor": scalefactor,
        'isMobile': is_mobile
    })
    await page.goto(url)
    time.sleep(delay)
    await page.screenshot({'path': dst, "fullPage": fullpage})
    await browser.close()


if __name__ == '__main__':
    args = argument.Arguments()
    args.always("url")
    args.option("dst", "example.png")
    args.option("width", 375)
    args.option("height", 667)
    args.option("scalefactor", 1.0)
    args.option("delay", 0)
    args.option("is_mobile", True)
    args.switch("fullpage")

    args.process("width", lambda x: int(x))
    args.process("height", lambda x: int(x))
    args.process("scalefactor", lambda x: float(x))
    args.process("delay", lambda x: float(x))
    args.process("is_mobile", lambda x: bool(x))

    arg, err = args.parse()
    print(arg)
    if len(err) > 0:
        for e in err:
            print(e)
        print(args)
        sys.exit(120)
    asyncio.get_event_loop().run_until_complete(screenshot(**arg))
