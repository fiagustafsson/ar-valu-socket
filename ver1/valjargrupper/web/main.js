import cssMain from './main.scss';
import * as d3 from "d3";
import _ from 'lodash';

const data = require('./data.json');

var stateTarget, stateCurrent = "";

// set this in create
var svg, xAxis, yAxis, barContainer, innerSize, barContainer, partyBoxContainer;
var isCreated = false;
var counterChanges = 0;
var inUpdate = false;

const xAxisBarMargin = 50;
// settings
const numBars = 10;
const dur = 125; // transition duration
const xMax = 0.7;
const labelMarginRight = 30;
const margin = {
    top: 0,
    right: 0,
    bottom: 20,
    left: 0
};


function update(state) {
    if (inUpdate) {
        return
    }
    inUpdate = true;
    let delayEnter = dur * 2.5;
    let delayUpdate = dur * 1.5;
    let delayExit = 0;
    if (counterChanges == 0) {
        delayEnter = 0;
    }
    render(state, delayEnter, delayUpdate, delayExit)
    counterChanges += 1;
    stateCurrent = state;
    inUpdate = false;
}


var Chart = function(el, data, ratio, names){
    let nRows = data[0].items.length
    let w = el.clientWidth;
    let h = el.clientWidth * ratio;
    console.log(names);
    this.names = names; 
    this.el = el;
    this.svg =  d3.select(el).append("svg")
        .attr("width", w)
        .attr("height",h)
        .append("g")
    this.barContainer = this.svg.append('g').attr('class', 'bars');
    this.partyBoxContainer = this.svg.append('g').attr('class', 'partybox');

    this.yAxis = d3.scaleBand().rangeRound([0, h])
        .domain(Array.from(new Array(nRows), (x, i) => i))
        .paddingInner(0.25)
        .paddingOuter(0);
    
    this.yAxisInner = d3.scaleBand().rangeRound([0, this.yAxis.bandwidth()])
        .domain(Array.from(new Array(data.length), (x, i) => i))
        .paddingInner(0.25)
        .paddingOuter(0);

    this.xAxis = d3.scaleLinear().range([0, w])
        .domain([0, xMax]);

    this.partyBoxes = this.partyBoxContainer.selectAll(".partybox")
        .data(_.map(data[0]["items"], d=>d.party));
    
    this.bars = this.barContainer.selectAll(".bar")
        .data(data)

    let yAxis = this.yAxis;
    let ep = this.partyBoxes.enter();

        ep
        .append("rect")
        .attr("class", ((d, i) => "color-bg-party-s" + d))
        .attr("y", function(d, i) {
            return yAxis(i)
        })
        .attr("height", this.yAxis.bandwidth())
        .attr("style", "fill:#fff;stroke:#333;" )
        .attr("width", this.yAxis.bandwidth());

        ep.append("text")

        .attr("y", function(d, i) {
            return yAxis(i)
        })
        .text(function(d, i) {
            return d.toUpperCase()
        })
        .attr("alignment-baseline", "central")
        .attr("dominant-baseline", "central")
        .attr("text-anchor", "middle")
        .attr("style", "fill:#333;font-size:"+this.yAxis.bandwidth()/2 +"px" )
        .attr("y", function(d, i) {
            return yAxis(i) + yAxis.bandwidth() / 2
        })
        .attr("x", this.yAxis.bandwidth() / 2);

        this.render(data);
}

PartyChart.prototype.render = function (data) {
    console.log(data);
    let pc = this;
    let nd = data.length;
    for (var i0 = data.length - 1; i0 >= 0; i0--) {

        let r = this.svg.append('g')
        let b = r.selectAll(".row")
        .data(data[i0].items)
        .enter()

        b.append("rect")
        .attr("style", "stroke:#333333;fill:#ffffff;")
        .attr("y", function(d, i) {
            return pc.yAxis(i) +  pc.yAxisInner(i0)
        })
        .attr("x",  pc.yAxis.bandwidth() )
        .attr("height", pc.yAxisInner.bandwidth())
        .attr("width", function(d) {
            return pc.xAxis(Math.round(d.value * 100) / 100);
        });

        b.append("text")
        .attr("y", function(d, i) {
            // return pc.yAxis(i) + pc.yAxisInner() i0*pc.yAxis.bandwidth()/nd   + pc.yAxis.bandwidth()/nd/2
            return pc.yAxis(i) + pc.yAxisInner(i0) + pc.yAxisInner.bandwidth()/2;
         })
        // .attr("x", d =>  pc.yAxis.bandwidth() +pc.xAxis(Math.round(d.value * 100) / 100) + 5 )
        .attr("x", d =>  pc.yAxis.bandwidth() + pc.xAxis(d.value/2))
        .attr("height", pc.yAxisInner.bandwidth())
        .attr("alignment-baseline", "central")
        .attr("dominant-baseline", "central")
        .attr("text-anchor", "left")
        // .attr("fill", "#333333" )
        .attr("style", "fill:#333;font-size:"+pc.yAxisInner.bandwidth() +"px" )
        // .attr("font-size", pc.yAxis.bandwidth()/nd +"px" )
        .text((d,i) => { 
            if (i == 0){
                return "" +Math.round(d.value * 100) +"               " + pc.names[i0];
            }
            return '' +Math.round(d.value * 100);
        });

    }

}

function create(el, data, ratio,names) {
    return new PartyChart(el, data, ratio,names);

    // console.log(data);
    // // let el = document.getElementById(elementId);
    // let nRows = data[0].items.length
    // let w = el.clientWidth;
    // let h = el.clientWidth * ratio;

    // // d3 init
    // svg = d3.select(el).append("svg")
    //     .attr("width", w)
    //     .attr("height",h)
    //     .append("g")

    // barContainer = svg.append('g').attr('class', 'bars');
    // partyBoxContainer = svg.append('g').attr('class', 'partybox');

    // yAxis = d3.scaleBand().rangeRound([0, h])
    //     .domain(Array.from(new Array(nRows), (x, i) => i))
    //     .paddingInner(0.25)
    //     .paddingOuter(0);

    // xAxis = d3.scaleLinear().range([0, w])
    //     .domain([0, xMax]);

    // isCreated = true;
}


function render(state, delayEnter, delayUpdate, delayExit) {


    let fdata = _.map(state.parties, (x) => {
        return _.head(_.filter(data, (d) => {
            let q1 = (d.year === state.year);
            let q2 = (d.party === x);
            return q1 && q2;
        }));
    });
    fdata = _.map(fdata, (x) => {
        let r = {
            value: _.get(x, state.group, -1),
            id: _.get(x, "party", 0)
        }
        if (r.value == -1) {
            r.text = "Uppgift saknas",
                r.value = 0
        } else {
            r.text = '' + Math.round(r.value * 100) + "%";
        }
        return r;

    })

    // DATA BIND
    const partyBoxes = partyBoxContainer.selectAll(".partybox")
        .data(state.parties, function(d) {
            return d;
        })

    const bars = barContainer.selectAll(".bar")
        .data(fdata, function(d) {
            return d.id;
        })


    // UPDATE

    const update = bars.transition()
        .duration(dur);
    // .delay(delayUpdate);


    update
        .select(".bar-rect")
        .attr("y", function(d, i) {
            return yAxis(i)
        })

        .attr("width", function(d) {
            return xAxis(Math.round(d.value * 100) / 100);
        });

    update
        .select(".text-value")
        .attr("x", function(d) {
            if (d.value > 0) {

                return xAxis(Math.round(d.value * 100) / 100) + xAxisBarMargin + 10;
            }
            return xAxis(0) + xAxisBarMargin;
        })
        .text(function(d) {
            return d.text;
        })




    // ENTER
    const enterPartybox = partyBoxes
        .enter()
        .append("g")
        .attr("class", "partybox");

    enterPartybox.append("rect")
        .attr("class", ((d, i) => "color-bg-party-" + d))
        .attr("y", function(d, i) {
            return yAxis(i)
        })
        .attr("height", yAxis.bandwidth())
        .attr("width", yAxis.bandwidth());

    enterPartybox.append("text")

        .attr("y", function(d, i) {
            return yAxis(i)
        })
        .text(function(d, i) {
            return d.toUpperCase()
        })
        .attr("alignment-baseline", "central")
        .attr("dominant-baseline", "central")
        .attr("text-anchor", "middle")
        .attr("y", function(d, i) {
            return yAxis(i) + yAxis.bandwidth() / 2
        })
        .attr("x", yAxis.bandwidth() / 2)

    const enter = bars
        .enter()
        .append("g")
        .attr("class", "bar");


    let g1 = enter.append("g");

    g1.attr("class", ((d, i) => "bar-rect-cont bar-rect-cont-" + i + " color-bg-party-" + d.id))
        .append("rect")
        .attr("class", ((d, i) => "bar-item bar-rect bar-rect-" + i))
        .attr("y", function(d, i) {
            return yAxis(i)
        })
        .attr("x", xAxisBarMargin)
        //.attr("height", barHeight)
        .attr("height", yAxis.bandwidth())
        .transition()
        .duration(dur)
        .delay(delayEnter)
        .attr("width", function(d) {
            return xAxis(Math.round(d.value * 100) / 100);
        });

    g1.append("text")

        .attr("class", "text-value")
        .attr("y", function(d, i) {
            return yAxis(i)
        })
        // .attr("x", function(d) {
        // return xAxis(Math.round(d.value * 100) / 100) + xAxisBarMargin + 10;
        // })
        .attr("x", function(d) {
            if (d.value > 0) {

                return xAxis(Math.round(d.value * 100) / 100) + xAxisBarMargin + 10;
            }
            return xAxis(0) + xAxisBarMargin;
        })
        .attr("height", yAxis.bandwidth())
        .attr("alignment-baseline", "central")
        .attr("dominant-baseline", "central")
        .attr("text-anchor", "right")
        .attr("y", function(d, i) {
            return yAxis(i) + yAxis.bandwidth() / 2
        })
        .text(x => { return x.text });
    // EXIT

    const exit = bars.exit()
        .transition()
        .duration(dur)
        .delay(delayExit);

    exit
        .select(".bar-rect")
        .attr("width", 0);
    exit
        .remove();
}




exports.update = update;
exports.create = create;