import _ from 'lodash';
import $ from 'jquery';
import cssFonts from './fonts.css';
import * as d3 from "d3";
import {create, update} from './main';
create("viz");

var stateParties = ["alla"];


var $buttons;
function init() {
	$buttons = $(".ui-valu-button-container button");

	$buttons.on("click",(x) => {

		let d = $(x.target);
		let p = ''+d.data("party");
		if (_.includes(stateParties, p)) {
		    _.remove(stateParties, function(x) { return x === p})

		} else {
            stateParties.push(p);
        }

		updateYear();
	});	

	updateYear();

}
function updateYear(){
	$buttons.removeClass("lit");

	var $activeButtonYear = $buttons.filter(function() { 
	  return _.includes(stateParties, $(this).data("party"));
	});
    $activeButtonYear.addClass("lit");



	update(stateParties);

}

$(document).ready( function () {
	init();
});
