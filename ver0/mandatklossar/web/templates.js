import Handlebars from 'handlebars';
const templateStr =  `
{{#each items as |row|}}
    <div class="board-row" data-name="{{row.name}}">
    <h3>{{row.name}} <em>{{cleanProcent row.total}}</em></h3>
        <div class="board-row-container">
        {{#each row.items as |item|}}
        <div class="board-item color-bg-party-{{item.party}} js-clickselect"  draggable="true" data-party="{{item.party}}" style="height:{{ item.sz }}%">
            {{ funUpper item.party}}
        </div>
        {{/each}}
        </div>
       
    </div>
{{/each}}
`;

Handlebars.registerHelper('funUpper', function(str) {
  return str.toUpperCase();
});

Handlebars.registerHelper('cleanProcent', function(d) {
  let z = parseFloat(d ||"0");
  if (z > 0 ){
    return Math.round(10*z)/10 + "%";
  }
  return ""
});


var template = Handlebars.compile(templateStr);

function renderBoard(ctx){
    return template(ctx);
}

export {renderBoard}