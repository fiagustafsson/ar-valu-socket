import _ from 'lodash';
import $ from 'jquery';
import cssFonts from './fonts.css';
import * as d3 from "d3";
import { create, update } from './main';
create("viz");

var stateGroup = "svensk_ekonomi";

var $selectGroup;

function init() {
    $selectGroup = $(".js-select-group");
    $selectGroup.on("change", (x) => {
        let $d = $(x.target);
        stateGroup = '' + $d.val();
        updateState();
    });
    $selectGroup.val(stateGroup);
    updateState();
}

function updateState() {


    let context = {
        "group": stateGroup,
        'parties': ["v", "s", "mp", "c", "l", "kd", "m", "sd"]
    }
    $("#subtitle").text($selectGroup.find(":selected").text());
    update(context);
}


$(document).ready(function() {
    init();

})