import * as d3 from "d3";
import _ from 'lodash';
import { colors } from '../../../styles/basevalu';


//containers
var svg, mContainer;

// axis
var xAxis, yAxis;



//state
var counterChanges = 0;

// settings
const numBars = 4;
const dur = 125; // transition duration
const xMax = 1; // values goes from 0 - 1
const marginN = 20;
const manSize = 5;
//      margin and paddings
var innerSize;
const xAxisBarMargin = 0;
const labelMarginRight = 0;
const margin = {
    top: 0,
    right: 0,
    bottom: 0,
    left: 0
};


const mRows = 3;
const mCols = 2;

var mandates = [];

var gridScale;
var radius = 5;
var data;


function create(elementId, dd) {
    data = dd;
    let el = document.getElementById(elementId);
    let w = el.clientWidth;
    let h = el.clientHeight;
    innerSize = {
        width: w - margin.right - margin.left,
        height: h - margin.top - margin.bottom,
    }

    let gC = 44;
    let gR = 8;

    // d3 init
    let ratio = gR / gC;
    let viewBoxX = 640;
    svg = d3.select(el).append("svg")
        .attr("viewBox", "0 0 " + viewBoxX + " " + viewBoxX * ratio)
        .attr("width", w)
        .attr("height", w * ratio)
        // .attr("transform", "rotate(90)")
        .append("g")
        .attr("id", "svg-mandate")

    mContainer = svg.append('g').attr('class', 'mandates');

    let N = 12;

    let r = -1;
    let c = 0;

    gridScale = d3.scaleLinear()
        .range([radius, viewBoxX - radius])
        .domain([0, gC]);


    for (var i = 0; i < 349; i++) {
        let o = {
            'id': i,
            'party': '',
        }
        r = r + 1;
        if (r == gR) {
            c += 1;
            r = 0;
        }
        let nn = 0.5;
        if (i == 168) {
            r += 1;
        }
        if (i == 174) {
            c += 1;
            r = 3.5;
        }
        if (i == 175) {
            r = 1;
            c += 1;
        }
        if (i == 181) {
            r = 0;
            c += 1;
        }
        o.row = r;
        o.col = c;
        mandates.push(o);
    }


}

function update(state) {
    render(state);
    counterChanges += 1;
}


function render(state) {
    let ar = data["result"][state.group];
    let z = [];
    let xx = _.map(state.group, d => {
        let p = data["result"][d];
        p["n"] = 0;
        return p
    });
    xx = _.filter(xx, d => d.mandate > 0);
    let ix = 0;
    let fdata = _.map(mandates, (d, i) => {
        d.party = "-";
        d.class = ""
        if (xx.length > ix) {
            let z0 = xx[ix];
            if (z0.n < z0.mandate) {
                z0.n += 1;
                d.party = z0.party;
            }
            if (z0.n == z0.mandate) {
                ix += 1;
            }

        }
        return d;
    })
    var sMandate = mContainer.selectAll(".mandate").data(fdata);
    sMandate.enter()
        .append("circle")
        .attr("r", radius)
        // .attr("stroke", "#ffffff10")
        // .attr("stroke-width", "1")
        .attr("cy", function(d, i) {
            return gridScale(d.row);
        })
        .attr("cx", function(d, i) {

            return gridScale(d.col);
        })
        .attr("class", d => { return "mandate" })
        .style("fill", d => { return colors.party[d.party] });
    sMandate.transition().style("fill", d => { return colors.party[d.party] });
    sMandate.selectAll(".mandate").exit().remove();
}

exports.update = update;
exports.create = create;
