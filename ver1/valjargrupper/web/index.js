import _ from 'lodash';
import $ from 'jquery';
import cssFonts from './fonts.css';
import * as d3 from "d3";
import { create, update } from './main';
const data = require('./data.json');



var stateYear = "2014";
var stateGroup = "women";

var $buttonsYear, $selectGroup;
const parties = ["v", "s", "mp", "c", "l", "kd", "m", "sd"];

function pluckData(year, series){
    let fdata = _.filter(data, d=> d.year == year);
    let res = [];
    
    _.map(series, s => {
        let r =  {};
        let s0 = _.map(fdata, d=>{ return {'party': d["party"], 'value':d[s], 'sortParty':parties.indexOf(d["party"]) } });
        res.push({
            "name": s,
            "items": _.filter(_.sortBy(s0, d=> d.sort),d=> d.sort != -1)
        })
    });
    console.log(res);
    return res; 
}

function init() {

    // create($("#viz")[0],data);
    $buttonsYear = $(".btn-group-year button");
    $selectGroup = $(".js-select-group");

    $buttonsYear.on("click", (x) => {
        let d = $(x.target);
        stateYear = '' + d.data("year");
        // updateState();
    });
    $selectGroup.on("change", (x) => {
        let $d = $(x.target);
        stateGroup = '' + $d.val();
        // updateState();
    });

    $selectGroup.val(stateGroup);
    let fdata = pluckData(stateYear, ["women", "men"])
    create($(".viz-gender")[0],fdata,2,["Kvinnor", "Män"]);

    let fdata2 = pluckData(stateYear, ["_65","_31_64","_22_30","_18_21"])
    create($(".viz-age")[0],fdata2,2,["65+","32 - 64","22 - 30","18 - 21"]);

    let fdata3 = pluckData(stateYear, ["tjansteman","arbetare","foretagare_jordbrukare"])
    create($(".viz-occ")[0],fdata3,2,["Tjänstemän","Arbetare","Företagare och jordbrukare"]);
    // updateState();

}

function updateState() {
    $buttonsYear.removeClass("lit");

    var $activeButtonYear = $buttonsYear.filter(function() {
        return $(this).data("year") == stateYear;
    });
    $activeButtonYear.addClass("lit");

    let context = {
        "year": stateYear,
        "group": stateGroup,
        'parties': ["v", "s", "mp", "c", "l", "kd", "m", "sd"],
        'years': [1991, 1994, 1998, 2002, 2006, 2010, 2014, 2018]
    }
    $("#subtitle").text($selectGroup.find(":selected").text() + ", " + $activeButtonYear.text());
    update(context);
}


$(document).ready(function() {
    init();
})