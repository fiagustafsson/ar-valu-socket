import datetime
import glob
import hashlib
import json
import os
import shutil
import subprocess
import time
from subprocess import Popen

import miniotaurus
import settings


def init_directories():
    try:
        shutil.rmtree("stage")
    except FileNotFoundError:
        pass
    os.mkdir("stage")
    for n in settings.names:
        os.mkdir(f"stage/{n}")


def resolve_path(name=""):
    s = os.path.abspath(__file__)
    a, b = os.path.split(s)
    p = "/".join(a.split("/")[:-1])
    pth = os.path.join(p, name)
    res = {'index.html': os.path.join(pth, "web/index.html")}
    return res


def build(obj):
    # pth = resolve_path(name)
    cmd = f'parcel build \'{obj["index.html"]}\' -d \'stage/{obj["name"]}\' --public-url . '
    print(cmd)
    p = Popen(cmd, shell=True)
    return p


def build_all():
    processes = [build(obj) for obj in settings.names["v0"]]
    t = time.time()
    blob = []
    blob.append(f"""
        <html>
        <head>
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <style>
            body {{ font-size:18px;}}
            a {{
            display:block;
                font-size:16px;
                padding:0.5em;
            }}
            </style>
        </head>
        </head>
        <body>
        <h1>Stage: {datetime.datetime.now().isoformat()}</h1>
    """)

    for n in settings.names["v0"]:
        blob.append(f'<a href="{n["name"]}/?t={t}">{n["name"]}</a>')
    blob.append(f"""

        </body></html>
    """)
    with open("stage/index.html", "w") as f:
        f.write("\n".join(blob))
    [x.wait() for x in processes]


def deploy():
    config = json.loads(open(os.path.expanduser("~/.qs/secret.json")).read())
    mt = miniotaurus.Miniotarus(config)
    mt.putdir("articledata", "valu-stage-hemlis", "stage")


def take_screenshots():
    p = Popen(["python", "-m", "http.server", "8090"], cwd="stage")
    t = time.time()
    for g in glob.glob("stage/*/index.html"):
        u = g.split("/")[1]
        try:
            os.makedirs(f"shotlog/{u}/")
        except OSError:
            pass
        url = f'http://localhost:8090/{u}/'
        dst = f'shotlog/{u}/{u}.{t}.png'
        cmd = f"python simplescreenshot.py --delay=0.5 --dst={dst} --fullpage {url}"
        z = subprocess.check_call(cmd, shell=True)
    p.terminate()


def compare_images():
    from PIL import Image
    from PIL import ImageChops
    import skimage.measure
    import math

    import imutils
    import cv2

    imgs = []
    ldir = ""
    for g in sorted(glob.glob("shotlog/*/*.png")):
        ld = g.split("/")[1]
        if ld != ldir:
            ldir = ld
            imgs = []
        imgs.insert(0, cv2.imread(g))
        # imgs.insert(0, Image.open(g))
        if len(imgs) == 1:
            continue

        imgs = imgs[0:2]
        x, y = imgs
        d0 = len(x) != len(y)
        d1 = len(x[0]) != len(y[1])
        if (d0 or d1):
            continue

        d = skimage.measure.compare_nrmse(x, y)
        if d < 0.05:
            os.remove(g)


def build_screenshot_page():
    blob = []
    blob.append(f"""
        <html>
        <head>
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <style>
            body {{ font-size:18px;}}
            a {{
            display:block;
                font-size:16px;
                padding:0.5em;
            }}
            </style>
        </head>
        </head>
        <body>
        <h1>Stage: {datetime.datetime.now().isoformat()}</h1>
    """)

    for g in sorted(glob.glob("shotlog/*/*.png")):
        n = g[len("shotlog/"):]
        blob.append(f'<img src="{n}"" />')

    blob.append(f"""
        </body></html>
    """)
    with open("shotlog/index.html", "w") as f:
        f.write("\n".join(blob))


def compress_screenshots():
    for g in sorted(glob.glob("shotlog/*/*.png")):
        subprocess.call(f"pngquant {g} -f --ext .png", shell=True)
        print(g)


def deploy_screenshots():
    config = json.loads(open(os.path.expanduser("~/.qs/secret.json")).read())
    mt = miniotaurus.Miniotarus(config)
    mt.putdir("articledata", "valu-stage-hemlis-screenshots", "shotlog")


def main():
    init_directories()
    build_all()
    deploy()
    take_screenshots()
    compare_images()
    build_screenshot_page()
    compress_screenshots()
    deploy_screenshots()


#

if __name__ == '__main__':
    main()
