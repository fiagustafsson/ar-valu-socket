import cssMain from './main.scss';
import * as d3 from "d3";
import _ from 'lodash';
import $ from 'jquery';
import {setState, sendToSocket, isCaspar, isViewMode} from "./index";

let parties = ["v","s","mp","c","l","m","kd","sd"];
let from = "nodes2010";
let to = "nodes2014";
let idToRemove = [];
let svg,
    innerSize,
    yLeft,
    yRight,
    linkWidth,
    linkWidth2,
    diagonal,
    leftBoxContainer,
    rightBoxContainer,
    leftBoxes,
    rightBoxes,
    DOMLinksContainer,
    labelContainer,
    w,
    h;

// State holding object
let state = {
    isLeft: null
};

const margin = {
    top: 10,
    right: 10,
    bottom: 5,
    left: 10
};

const boxPaddingInner = 0.1;
const boxPaddingOuter = 0;

let leftData;
let rightData;

const rowLeftLabelText = "2014";
const rowRightLabelText = "2018";

const linkMinWidth = 3;
let data;
let lastEntity = "";

function getData() {
    return {left: leftData, right: rightData};
}

function init(_parties, _from, _to) {
    parties = _parties ? _parties : parties;
    from = _from ? _from : from;
    to = _to ? _to : to;
}


function create(elementId, _data) {
    data = removeFromData(_data);
    leftData = sortArray(data[from]);
    rightData = sortArray(data[to]);

    // Append the links
    function appendLinkstoNodes(nodesArray) {
        _.each(nodesArray, function (nodes) {
            _.each(nodes, function (d) {
                let links = _.filter(data.links, {source: d._id});

                d.links = {};

                _.each(links, function (e) {
                    d.links[e.target] = e.value;
                });
            });
        });
    }

    appendLinkstoNodes([leftData, rightData]);

    let el = document.getElementById(elementId);
    let title = document.getElementById("title");
    w = el.clientWidth;
    h = el.clientHeight - title.clientHeight;

    let marginLeft =  Math.max(((w / 100) * margin.left), ((h / 100) * 5.5 * 3));
    let marginRight =  Math.max(((w / 100) * margin.right), ((h / 100) * 5.5 * 3));

    innerSize = {
        width: w - marginRight - marginLeft,
        height: h - ((h / 100) * margin.top) - ((w / 100) * margin.bottom)
    };

    d3.select(el).selectAll("svg").remove();

    svg = d3.select(el).append("svg")
        .attr("width", w)
        .attr("height", h)
        .append("g")
        .attr("id", "svg")
        .attr("transform", "translate(" + marginLeft + "," + ((h / 100) * margin.top)+ ")");

    yLeft = d3.scaleBand()
        .domain(_.map(leftData, (x) => {return x._id}))
        .range([0, innerSize.height])
        .paddingInner(boxPaddingInner)
        .paddingOuter(boxPaddingOuter)

    yRight = d3.scaleBand()
        .domain(_.map(rightData, (x) => {return x._id}))
        .range([0, innerSize.height])
        .paddingInner(boxPaddingInner)
        .paddingOuter(boxPaddingOuter);

    linkWidth = d3.scaleLinear()
        // .domain([1, d3.max(data.links, (function (d) {
        //     return d.value;
        // }))])
        // .range([5, yRight.bandwidth()]);
        .domain([0, 100])
        // .range([0, yRight.bandwidth()]);
        .range([linkMinWidth, yRight.bandwidth()]);


    diagonal = d3.linkHorizontal();

    leftBoxContainer = svg.append('g')
        .attr('class', 'left-boxes');

    rightBoxContainer = svg.append('g')
        .attr('class', 'right-boxes');

    DOMLinksContainer = svg.append("g")
        .attr("class", "domlinks");

    labelContainer = svg.append("g")
        .attr("class", "percent-labels");

    svg.append("g")
        .attr("class", "row-label row-label-left")
        .append("text")
        .attr("class", "row-label-text row-label-text-left")
        .text(rowLeftLabelText)
        .attr("x", yLeft.bandwidth()/2)
        .attr("y", -yLeft.bandwidth()/2)
        .attr("text-anchor", "middle")
        .attr("alignment-baseline", "central")
        .attr("dominant-baseline", "central");

    svg.append("g")
        .attr("class", "row-label row-label-right")
        .append("text")
        .attr("class", "row-label-text row-label-text-right")
        .text(rowRightLabelText)
        .attr("x", innerSize.width - yRight.bandwidth()/2)
        .attr("y", -yRight.bandwidth()/2)
        .attr("text-anchor", "middle")
        .attr("alignment-baseline", "central")
        .attr("dominant-baseline", "central");

    renderBoxes()

}

function sortArray(_data) {
  _data.sort(function(a, b) {
    if(parties.indexOf(a._short) < parties.indexOf(b._short)) {
      return -1;
    }
    return 1;
  });
  return _data;
}

function removeFromData(_data) {
  var newData = {};
  newData[from] = filterJson(_data[from]);
  newData[to] = filterJson(_data[to]);
  newData["links"] = removeLinks(_data["links"]);
  return newData;
}

function filterJson(_data) {
    return _data.filter(function(item) {
        if(parties.includes(item._short)) {
            return true;
        }
        idToRemove.push(item._id);
        return false;
    });
}

function removeLinks(_links) {
    return _links.filter(function(item) {
        if(!idToRemove.includes(item.target) && !idToRemove.includes(item.source)) {
            return true;
        } else {
            return false;
        }
    });
}

function renderBoxes() {

    // Create the party boxes
    leftBoxes = leftBoxContainer
        .selectAll('.left-box')
        .data(leftData, function(d) { return d._id});

    let leftEnter = leftBoxes
        .enter()
        .append('g')
        .attr('class', 'box left-box')
        .on('click', function (d) {
            if(!isCaspar || isViewMode)
                _update(d, true)
        });

    leftEnter.append('rect')
        .attr("height", yLeft.bandwidth())
        .attr("width", yLeft.bandwidth())
        .attr("y", function (d) {
            return yLeft(d._id)
        })
        .attr("x", 0)
        .attr("class", function (d) {
            return `party-box party-box-left color-bg-party-${d._short}`
        });


    leftEnter.append('text')
        .text(function (d) {
            return d._short.toUpperCase();
        })
        .attr('class', 'party-short')
        .attr("y", function (d) {
            return yLeft(d._id) + yLeft.bandwidth() / 2
        })
        .attr("x", yLeft.bandwidth() / 2)
        .attr("text-anchor", "middle")
        .attr("alignment-baseline", "central")
        .attr("dominant-baseline", "central");

    rightBoxes = rightBoxContainer
        .selectAll('.right-box')
        .data(rightData, function(d) { return d._id});

    let rightEnter = rightBoxes
        .enter()
        .append('g')
        .attr('class', 'box right-box')
        .on('click', function (d) {
            if(!isCaspar || isViewMode)
                _update(d, true)
        });

    rightEnter.append('rect')
        .attr("height", yRight.bandwidth())
        .attr("width", yRight.bandwidth())
        .attr("y", function (d) {
            return yRight(d._id)
        })
        .attr("x", innerSize.width - yRight.bandwidth())
        .attr("class", function (d) {
            return `party-box party-box-right color-bg-party-${d._short}`
        });


    rightEnter.append('text')
        .text(function (d) {
            return d._short.toUpperCase();
        })
        .attr('class', 'party-short')
        .attr("y", function (d) {
            return yRight(d._id) + yRight.bandwidth() / 2
        })
        .attr("x", innerSize.width - yRight.bandwidth() / 2)
        .attr("text-anchor", "middle")
        .attr("alignment-baseline", "central")
        .attr("dominant-baseline", "central");
}




// Updates state values and calls updating functions
function update(entity, start, send) {
    let nodelist = start === "left" ? leftData : rightData;

    let e = _.find(nodelist, function (x) {
        return x._short === entity
    });

    _update(e, send);
}

function _update(e, send) {
  if(lastEntity == e || !e) {
    lastEntity = "";
    e = {_short:""};
    reset();
  } else {
    state.isLeft = _.some(leftData, e); //checks if the update element is in the dataset for the left side
    let currentLinks = data.links.filter(function (d) {
      return d.source === e._id;
    });
    // sortOpposite updates domain of target side
    let targetData = sortOpposite(e);
    // use sorted target data to sort links, making sure the thickest link is displayed first
    currentLinks.sort(function(a, b) {
      return _.findIndex(targetData, {"_id": a.target}) - _.findIndex(targetData, {"_id": b.target})
    });
    lastEntity = e;

    updateBoxOrder();
    updateLinks(currentLinks, e);
    updatePercentages(currentLinks);

    var _position = state.isLeft ? "left" : "right";

    if(send) {
      sendToSocket({position: _position, party: e._short, id: _position+ "-" + e._short});
    }
  }
}

function reset() {
  DOMLinksContainer.selectAll('.link').transition()
    .delay(function (d, i) {
      return i * 20;
    })
    .duration(200)
    .attr("stroke-dashoffset", function() { return this.getTotalLength() + 10 })
    .remove();

  labelContainer.selectAll('.percentage-label')
    .transition()
    .duration(300)
    .attr("opacity", 0)
    .remove();
  sendToSocket({position: "", party: "", id: ""});
}

// Draw the links
function updateLinks(l, e) {

    let strokeWidth = function(d) {
        return d.value > 0 ? linkWidth(d.value) : 0;
    };

    let ySource = checkDirection(yLeft, yRight);
    let yTarget = checkDirection(yRight, yLeft);

    let DOMLinks = DOMLinksContainer.selectAll('.link')
        .data(l, function(d) { return `${d.source}-${d.target}`});

    let cumHeight = 0;


    let sumHeight = _.reduce(l, function(total, n) {
        // return total + Math.max(strokeWidth(n), 5)
        return total + Math.max(strokeWidth(n), linkMinWidth)
    }, 0);


    DOMLinks
        .enter()
        .append('path')
        .attr("class", function() {
            return `link ${e._short}`
        })
        .attr('stroke', "#fefefe")
        //.attr('stroke-width', d => Math.max(strokeWidth(d), 5))
        .attr('stroke-width', strokeWidth)
        .attr('fill', 'none')
        .attr('d', function(d, i) {
            let xSoffset = checkDirection(-1, 1);
            let xS = checkDirection(ySource.bandwidth(), innerSize.width - yTarget.bandwidth()) + xSoffset;
            let xT = checkDirection(innerSize.width - ySource.bandwidth(), yTarget.bandwidth());
            // let yS = ySource(d.source) + ySource.bandwidth()/2;
            let yT =  yTarget(d.target) + yTarget.bandwidth()/2;
            let yS = ySource(d.source) + strokeWidth(d)/2 + cumHeight;
            let ST = {"source": [xS, yS], "target": [xT, yT] };
            let offset = (sumHeight - ySource.bandwidth()) / (l.length-1);
            cumHeight += strokeWidth(d) - offset;
            return diagonal(ST)
        })
        // trick to create line drawing effect
        .attr("stroke-dasharray", function() { return (this.getTotalLength() + 10) + " " + (this.getTotalLength() + 10) })
        .attr("stroke-dashoffset", function() { return this.getTotalLength() + 10 })
        .transition()
        .delay(function (d, i) {
            return i * 80 + 700;
            // return 1000
        })
        .duration(500)
        .attr("stroke-dashoffset", 0);



    DOMLinks.exit()
        .transition()
        .delay(function (d, i) {
            return i * 20;
        })
        .duration(200)
        .attr("stroke-dashoffset", function() { return this.getTotalLength() + 10 })
        .remove();
}

function updatePercentages(l) {
    let percentageLabels = labelContainer.selectAll('.percentage-label')
        .data(l, function(d) { return d.target});

    let yTarget = checkDirection(yRight, yLeft);

    percentageLabels
        .text(function (d) {
            return d.value + '%';
        })
        .transition()
        .duration(1000)
        .attr('y', function (d) {
            return yTarget(d.target) + yTarget.bandwidth()/2
        });

    percentageLabels.enter()
        .append('text')
        .attr('class', 'percentage-label')
        .attr('x', function () {
            return checkDirection((innerSize.width + yRight.bandwidth()/1.2), (-yLeft.bandwidth()/1.2));
        })
        .attr('y', function (d) {
            return yTarget(d.target) + yTarget.bandwidth()/2
        })
        .text(function (d) {
            return d.value + '%';
        })
        .attr("opacity", 0)
        .attr("text-anchor", "middle")
        .attr("alignment-baseline", "central")
        .attr("dominant-baseline", "central")
        .transition()
        .duration(300)
        .attr("opacity", 1);

    percentageLabels.exit()
        .transition()
        .duration(300)
        .attr("opacity", 0)
        .remove();
}


// Performs the actual reordering of boxes on the other side
function updateBoxOrder() {

    let c = checkDirection('.right-box', '.left-box');
    let s = checkDirection(yRight, yLeft);

    svg.selectAll(c).select("rect")
        .transition()
        .duration(1000)
        .attr("y", function (d) {
            return s(d._id)
        });

    svg.selectAll(c).select("text")
        .transition()
        .duration(1000)
        .attr("y", function (d) {
            return s(d._id) + s.bandwidth()/2
        })

}

// Sorts box data on the other side based on the clicked box
function sortOpposite(e) {

    let targetData = checkDirection(rightData, leftData);

    // Sort the data based on current object data
    targetData
        .sort(function (a, b) {
            return e.links[b._id] - e.links[a._id];
        });

    let targetAxis = checkDirection(yRight, yLeft);
    targetAxis.domain(_.map(targetData, (x) =>  { return x._id }));

    return targetData
}

function checkDirection(forward, backward) {
    return state.isLeft ? forward : backward;
}


exports.create = create;
exports.update = update;
exports.initValjarstrom = init;