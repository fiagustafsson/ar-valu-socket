import glob
import os

names = {'v0': []}
for g in sorted(glob.glob("../ver0/*")):
    n = g.split("/")[-1]
    p = os.path.abspath(g)
    o = {
        'name': n,
        'root': p,
        'index.html': os.path.join(p, "web/index.html"),
    }
    names["v0"].append(o)
