import _ from 'lodash';
import $ from 'jquery';
import cssFonts from './fonts.css';
import * as d3 from "d3";
import {create, show, states} from './main';
create("viz");

var stateYear = "2018";

var $buttons;
function init() {
	$buttons = $(".btn-group-year button");

	$buttons.on("click",(x) => {
		let d = $(x.target);
		console.log(d);
		stateYear = ''+d.data("year"); 
		updateYear();
	});	

	updateYear();

}
function updateYear(){
	$buttons.removeClass("lit");

	var $activeButtonYear = $buttons.filter(function() { 
	  return $(this).data("year") ==  stateYear;
	});
	$activeButtonYear.addClass("lit");

	show(stateYear);

}

$(document).ready( function () {
	init();
})